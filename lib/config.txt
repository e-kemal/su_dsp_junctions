﻿
kuid                                    <kuid2:216981:60022:0>
kind                                    "library"
username                                "sU_DSP_junctions_lib"
category-class                          "YL"
category-region                         "BY;RU;SU;UA"
category-era                            "1960s;1970s;1980s;1990s;2000s;2010s"
script                                  "sudspjunctionslib.gs"
class                                   "suDSPJunctionsLib"
trainz-build                            3.7
license                                 

thumbnails
{
  0
  {
    image                               "thumbnail.jpg"
    width                               240
    height                              180
  }
}

string-table
{
  description                           "Правило для ручного переключения ручных стрелок"
  tcm-browser                           "Стрелки - ТЧМ"
  dsp-browser                           "Стрелки - ДСП"
  usericonmenu-tooltip                  "sU DSP Junctions"
}

script-include-table
{
  0                                     <kuid2:216981:60021:1>
}

kuid-table
{
  usericonmenu                          <kuid:401543:1185>
}