include "library.gs"
include "multiplayergame.gs"

class suDSPJunctionsLib isclass Library{
	Asset self;
	StringTable ST;
	ScenarioBehavior rule;

	void UpdateSystemIcon(void){
		if (Interface.GetDeviceFormFactor() == Interface.FormFactor_Phone)
			return;

		if (World.GetCurrentModule() == World.DRIVER_MODULE and rule)
			AddSystemMenuIcon(self.FindAsset("usericonmenu"), ST.GetString("usericonmenu-tooltip"), "");
		else
			RemoveSystemMenuIcon();
	}

	public string LibraryCall(string function, string[] stringParam, GSObject[] objectParam){
		int i, n;

		if (function == "RegisterScenarioBehavior"){
			if((objectParam.size()>0) and (rule = cast<ScenarioBehavior> objectParam[0])){
				UpdateSystemIcon();
			}
			return "OK";
		}
		return inherited(function, stringParam, objectParam);
	}

	public void OnModuleInit(Message msg){
		UpdateSystemIcon();
	}

	public void OnClickSystemButton(Message msg){
		Menu iconMenu = Constructors.NewMenu();
		iconMenu.AddItem(ST.GetString("tcm-browser"), rule, "suDSPJunctionsRule", "RequestTCMbrowser");
		iconMenu.AddItem(ST.GetString("dsp-browser"), rule, "suDSPJunctionsRule", "RequestDSPbrowser");
		if(!MultiplayerGame.IsActive() or MultiplayerGame.IsServer()){
			iconMenu.AddSeperator();
			iconMenu.AddItem("Log", me, "suDSPJunctionsRule", "RequestLOGbrowser");
		}
		ShowSystemMenuIconMenu(iconMenu);
	}

	public void Init(Asset asset){
		inherited(self=asset);
		ST=self.GetStringTable();

		RemoveSystemMenuIcon();
		AddHandler(me, "World", "ModuleInit", "OnModuleInit");
		AddHandler(me, "Interface", "ClickSystemButton", "OnClickSystemButton");
	}
};
