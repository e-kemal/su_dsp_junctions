@echo off
set gsc=D:\TS12\bin\trainzutil.exe
set include=D:\TS12\scripts\
set include2=..\..\z7_maincontroller\

for %%a in (*.gs) do %gsc% compile %%a -i%include% -i%include2%

del *.gsl

set gsc=
set include=
pause