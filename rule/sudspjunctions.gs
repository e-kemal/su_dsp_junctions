include "ScenarioBehavior.gs"
include "BinarySortedArray.gs"
include "multiplayergame.gs"
include "utilitylibrary.gs"
include "zx_dsp_core.gs"
include "externalcontroll.gs"
include "multiplayersessionmanager.gs"

class LogEntry{
	public define int CODE_TCM_SWITCH			=  0;	//переключение стрелки машинистом
//	public define int CODE_DSP_SWITCH			=  1;	//переключение стрелки дежурным
	public define int CODE_VSP_USE				=  1;	//нажатие на кнопку вспомогательного перевода
	public define int CODE_VSP_UNSEAL			=  2;	//срыв пломбы вспомогательного перевода
	public define int CODE_VSP_SEAL				=  3;	//восстановление пломбы вспомогательного перевода
	public define int CODE_INCISION				=  4;	//взрез стрелки
	public define int CODE_UNINCISION			=  5;	//восстановление стрелки после взреза
	public define int CODE_POST_CANCEL_USE		=  6;	//нажатие на кнопку отмены манёвров
	public define int CODE_POST_CANCEL_UNSEAL	=  7;	//срыв пломбы отмены манёвров
	public define int CODE_POST_CANCEL_SEAL		=  8;	//восстановление пломбы отмены манёвров
	public define int CODE_POST_PERMISSION_ON	=  9;	//разрешение манёвров
	public define int CODE_POST_PERMISSION_OFF	= 10;	//отмена разрешения манёвров
	public define int CODE_POST_TAKE_ON			= 11;	//восприятие разрешения манёвров
	public define int CODE_POST_TAKE_OFF		= 12;	//отмена восприятия разрешения манёвров
	public define int CODE_POST_SWITCH			= 13;	//перевод стрелки с поста местного управления
	public define int CODE_SPAN_ADD_DEP_USE		= 14;	//нажатие на кнопку вспомогательного отправления
	public define int CODE_SPAN_ADD_DEP_UNSEAL	= 15;	//срыв пломбы вспомогательного отправления
	public define int CODE_SPAN_ADD_DEP_SEAL	= 16;	//восстановление пломбы вспомогательного отправления
	public define int CODE_SPAN_ADD_APPR_USE	= 17;	//нажатие на кнопку вспомогательного приёма
	public define int CODE_SPAN_ADD_APPR_UNSEAL	= 18;	//срыв пломбы вспомогательного приёма
	public define int CODE_SPAN_ADD_APPR_SEAL	= 19;	//восстановление пломбы вспомогательного приёма
	public float time;
	public string user;
	public int code;
	public int item;
	public string info;
};

class suDSPJunctions isclass ScenarioBehavior, zxExtraLinkBase, zx_DSP_Extension {
	public Browser DSPbrowser=null;
	Browser LOGbrowser=null;
	Asset self;
	BinarySortedArray inSignals;
//	Library suDSPJunctionsLib;
	public MultiplayerSessionManager mpsessionlib;
	public UtilityLibrary REutility; //Ссылка на библиотеку утилит
	zxLibruary_core suCore; //Библиотека сигнализации
	zx_DSP_Library REdspLib;
	zx_DSP_Core REdspCore;
	StringTable ST;
	public int StationIndex=-1;//активная "вкладка" станции
	LogEntry[] Log=new LogEntry[0];
	ExternalControll[] posts=new ExternalControll[0];
	SecurityToken junctionToken;

	bool propertiesIsLoading = false;

	public define int J_MANUAL	= 1;//Ручная стрелка (НЕ в централизации)
	public define int J_PLUS	= 2;//стрелочная рукоятка в положении +
	public define int J_MINUS	= 4;//стрелочная рукоятка в положении -
	public define int J_SET_MANUAL	= J_PLUS+J_MINUS;//стрелочная рукоятка в одном из крайних положений
	public define int J_RED_CAP	= 8;//Красный колпачок
	public define int J_VSP_SEAL	= 16;//Сорвана пломба вспомогательного перевода стрелки
	public define int J_INCISION	= 32;//Стрелка взрезана (нет контроля положения)
	public define int J_EXTERNAL	= 64;//Стрелка замкнута внешним постом (возможно как охранная)

	public define int SPAN_TYPE			= 3;	//маска для типа
	public define int SPAN_TYPE_NONE	= 0;	//нет
	public define int SPAN_TYPE_TOKEN	= 1;	//ЭЖС
	public define int SPAN_TYPE_PAB		= 2;	//ПАБ
	public define int SPAN_TYPE_AB		= 3;	//АБ

	public define int SPAN_TYPE_NOCONFIRM	= 4;	//Разворот перегона с одной кнопки / автоподтверждение прибытия (ССО)
	public define int SPAN_TYPE_KEY			= 8;	//наличие ключа-жезла

	public define int SPAN_AB_STATE_DEP				=   1;	//АБ на отправление
	public define int SPAN_AB_STATE_KEY				=   2;	//изъят ключ-жезл
	public define int SPAN_AB_STATE_BTN_CONFIRM		=   4;	//нажата кнопка согласия на разворот перегона
	public define int SPAN_AB_STATE_BTN_DEP			=   8;	//нажата кнопка разворота перегона
	public define int SPAN_AB_STATE_BTN_ADD_APPR	=  16;	//нажата кнопка вспомогательного приёма
	public define int SPAN_AB_STATE_BTN_ADD_DEP		=  32;	//нажата кнопка вспомогательного отправления
	public define int SPAN_AB_STATE_BUTTONS			=  SPAN_AB_STATE_BTN_CONFIRM | SPAN_AB_STATE_BTN_DEP | SPAN_AB_STATE_BTN_ADD_APPR | SPAN_AB_STATE_BTN_ADD_DEP;
	public define int SPAN_AB_STATE_SEAL_ADD_APPR	=  64;	//сорвана пломба с кнопки вспомогательного приёма
	public define int SPAN_AB_STATE_SEAL_ADD_DEP	= 128;	//сорвана пломба с кнопки вспомогательного отправления

	public define int SPAN_PAB_CONFIRM	= 1;	//согласие на отправление поезда
	public define int SPAN_PAB_APPROACH	= 2;	//путевое прибытие

	public void UpdateDSPbrowser(void){
		if(!DSPbrowser)return;
		int i, n=0;
		string ret="stations: ";
		for(i=0;i<REdspCore.stations.size();i++){
			if(REdspCore.stations[i].availableControl or !MultiplayerGame.IsActive() or MultiplayerGame.IsServer()){
				if(n)ret=ret+", ";
				if(i==StationIndex)
					ret=ret+"<b>"+REdspCore.stations[i].name+"</b>";
				else
					ret=ret+"<a href='live://setstation/"+i+"'>"+REdspCore.stations[i].name+"</a>";
				n++;
			}
		}
		if(!n){
			ret="No avalible stations<br><a href='live://refresh'>refresh</a>";
			DSPbrowser.LoadHTMLString(self, ret);
			return;
		}
		if(StationIndex<0 or StationIndex>=REdspCore.stations.size() or (!REdspCore.stations[StationIndex].availableControl and MultiplayerGame.IsActive() and !MultiplayerGame.IsServer())){
			ret=ret+"<br>station not selected<br><a href='live://refresh'>refresh</a>";
			DSPbrowser.LoadHTMLString(self, ret);
			return;
		}

		ret=ret+"<br>Стрелочные посты:<br>";
		for(i=0;i<posts.size();i++){
			if(posts[i].stationIndex != StationIndex)
				continue;
			ExternalControll P=posts[i];
			P.REdspCore=REdspCore;
//			P.stationIndex=StationIndex;
			ret=ret+P.name+"<br>";
			P.index=i;
			ret = ret + P.GetContentDSP() + "<br>";
		}
		ret=ret+"<br>Перегоны:<br>";
		for(i=0;i<REdspCore.stations[StationIndex].signals.size();i++){
			zxSignal_main sig=cast<zxSignal_main>REdspCore.stations[StationIndex].signals[i];
			if(!(sig.Type&zxSignal.ST_IN))continue;
			ret = ret + "<font color=\"#";
			if (sig.GetSignalState() == Signal.GREEN) ret = ret + "00ff00";
			else if (sig.GetSignalState() == Signal.YELLOW) ret = ret + "ffff00";
			else ret = ret + "ff0000";
			ret = ret + "\">*</font> " + sig.privateName + " - ";
			BinarySortedElement sigElement = inSignals.findElement(sig.GetId());
			if (!sigElement) {
				continue;
			}
			BinarySortedElement sigElement2 = null;
			zxSignal sig2 = null;
			Soup span = sig.span_soup;
			if (span and span.GetNamedTagAsBool("Inited", false)) {
				ret = ret + span.GetNamedTag("end_sign_s");
				sig2 = cast<zxSignal>Router.GetGameObject(span.GetNamedTag("end_sign"));
				if (sig2) {
					sigElement2 = inSignals.findElement(sig2.GetId());
				}
				else {
					ret = ret + " <font color=\"#ff0000\">" + span.GetNamedTag("end_sign") + " not found</font>";
				}
			}
			else
				ret = ret + "<i>???</i>";
			ret = ret + "<br>";
			if (!sigElement2)
				continue;

			int type = sigElement.type & SPAN_TYPE;
			int type2 = sigElement2.type & SPAN_TYPE;

			if (type == SPAN_TYPE_AB and type2 == SPAN_TYPE_AB) {
				ret = ret + "<font color=\"#";
				if	(sigElement.state & SPAN_AB_STATE_DEP)
					ret = ret + "00ff00";
				else
					ret = ret + "000000";
				ret = ret + "\">&gt;&gt;&gt;</font>&nbsp;<font color=\"#";
				if (!(sigElement.state & SPAN_AB_STATE_DEP))
					ret = ret + "ffff00";
				else
					ret = ret + "000000";
				ret = ret + "\">&lt;&lt;&lt;</font><br>";
				if (sigElement.state & SPAN_AB_STATE_DEP)
					span = sig2.span_soup;
				int n = span.GetNamedTagAsInt("extra_sign", 0);
				int j;
				string spanStr = "";
				for (j = 0; j < n; ++j) {
					string str = "";
					zxSignal_main prohodnoy = cast<zxSignal_main>Router.GetGameObject(span.GetNamedTag("sub_sign_"+j));
					if (prohodnoy) {
						str = str + prohodnoy.privateName + ": <font color=\"#";
						if (prohodnoy.GetSignalState() == Signal.GREEN) str = str + "00ff00";
						else if (prohodnoy.GetSignalState() == Signal.YELLOW) str = str + "ffff00";
						else str = str + "ff0000";
						str = str + "\">*</font>";
					}
					else {
						str = str + "???";
					}
					if (sigElement.state & SPAN_AB_STATE_DEP) {
						if (j)
							str = str + ", ";
						spanStr = str + spanStr;
					}
					else {
						if (j)
							spanStr = spanStr + ", ";
						spanStr = spanStr + str;
					}
				}
				ret = ret + spanStr + "<br>";
				ret = ret + "<a href='live://span" + sigElement.key + "/dep'>";
				if (sigElement.state & SPAN_AB_STATE_BTN_DEP)
					ret = ret + "<b>";
				ret = ret + "Смена направления";
				if (sigElement.state & SPAN_AB_STATE_BTN_DEP)
					ret = ret + "</b>";
				ret = ret + "</a><br>";
				if (!(sigElement2.type & SPAN_TYPE_NOCONFIRM)) {
					ret = ret + "<font color=\"ffff00\"><a href='live://span" + sigElement.key + "/confirm'>";
					if (sigElement.state & SPAN_AB_STATE_BTN_CONFIRM)
						ret = ret + "<b>";
					ret = ret + "Дача согласия";
					if (sigElement.state & SPAN_AB_STATE_BTN_CONFIRM)
						ret = ret + "<b>";
					ret = ret + "</a></font><br>";
				}
				if (sigElement.state & SPAN_AB_STATE_SEAL_ADD_APPR) {
					ret = ret + "<a href='live://span" + sigElement.key + "/addappr'>";
					if (sigElement.state & SPAN_AB_STATE_BTN_ADD_APPR)
						ret = ret + "<b>";
				}
				else
					ret = ret + "<i>";
				ret = ret + "Вспомогательный приём";
				if (sigElement.state & SPAN_AB_STATE_SEAL_ADD_APPR) {
					if (sigElement.state & SPAN_AB_STATE_BTN_ADD_APPR)
						ret = ret + "</b>";
					ret = ret + "</a>";
				}
				else
					ret = ret + "</i>";
				ret = ret + "&nbsp;<a href='live://span" + sigElement.key + "/addapprseal'>*</a><br>";
				if (sigElement.state & SPAN_AB_STATE_SEAL_ADD_DEP) {
					ret = ret + "<a href='live://span" + sigElement.key + "/adddep'>";
					if (sigElement.state & SPAN_AB_STATE_BTN_ADD_DEP)
						ret = ret + "<b>";
				}
				else
					ret = ret + "<i>";
				ret = ret + "Вспомогательное отправление";
				if (sigElement.state & SPAN_AB_STATE_SEAL_ADD_DEP) {
					if (sigElement.state & SPAN_AB_STATE_BTN_ADD_DEP)
						ret = ret + "</b>";
					ret = ret + "</a>";
				}
				else
					ret = ret + "</i>";
				ret = ret + "&nbsp;<a href='live://span" + sigElement.key + "/adddepseal'>*</a><br>";
			}
			else if (type == SPAN_TYPE_PAB and type2 == SPAN_TYPE_PAB) {
				ret = ret + "<font color=\"#";
				if	(sigElement2.state & 1)
					ret = ret + "00ff00";
				else
					ret = ret + "000000";
				ret = ret + "\">&gt;&gt;&gt;</font>&nbsp;<font color=\"#";
				if (sigElement2.state & 2)
					ret = ret + "ff0000";
				else
					ret = ret + "000000";
				ret = ret + "\">&gt;&gt;&gt;</font><br>";
				ret = ret + "<font color=\"#";
				if	(sigElement.state & 1)
					ret = ret + "ffff00";
				else
					ret = ret + "000000";
				ret = ret + "\">&lt;&lt;&lt;</font>&nbsp;<font color=\"#";
				if (sigElement.state & 2)
					ret = ret + "ff0000";
				else
					ret = ret + "000000";
				ret = ret + "\">&lt;&lt;&lt;</font><br>";
				ret = ret + "<font color=\"ffff00\"><a>Дача согласия</a></font><br>";
				ret = ret + "<a>Отмена согласия</a><br>";
				ret = ret + "<font color=\"#";
				if (sigElement.state & 4)
					ret = ret + "ffffff";
				else
					ret = ret + "000000";
				ret = ret + "\">*</font>&nbsp;<a>Дача прибытия</a><br>";
				ret = ret + "<a>Отправление хозяйственного поезда</a><br>";
				ret = ret + "<i>Искусственная фиксация прибытия</i>&nbsp;<a>*</a>&nbsp;0<br>";
			}
			ret = ret + "<br>";
		}

		ret=ret+"<br><a href='live://refresh'>refresh</a>";
		DSPbrowser.LoadHTMLString(self, ret);
	}

	void UpdateLOGbrowser(void){
		if(!LOGbrowser)return;
		string ret="junctions log<br>";
		string msg;
		string st;
		if(MultiplayerGame.IsActive() and !MultiplayerGame.IsServer()){
			ret=ret+"For admin only!";
			LOGbrowser.LoadHTMLString(self, ret);
			return;
		}
		int i, n=Log.size();
		ret=ret+"<table width='100%'><tr><td>time</td><td>user</td><td>message</td></tr>";
		for(i=n-1;i>=0;i--){
			ret=ret+"<tr><td>"+HTMLWindow.GetFloatAsTimeString(Log[i].time)+"</td><td>"+Log[i].user+"</td><td>";
			BinarySortedElement sigElement;
			zxSignal sig;
			switch(Log[i].code){
				case LogEntry.CODE_TCM_SWITCH:
				case LogEntry.CODE_VSP_USE:
				case LogEntry.CODE_VSP_UNSEAL:
				case LogEntry.CODE_VSP_SEAL:
				case LogEntry.CODE_INCISION:
				case LogEntry.CODE_UNINCISION:
				case LogEntry.CODE_POST_SWITCH:
					if(REdspCore and Log[i].item>=0 and Log[i].item<REdspCore.jncs.size()){
						msg="<a href=\"live://jnc/"+Log[i].item+"\">"+REdspCore.jncs[Log[i].item].number+"@";
						int j=0;
						while(j<REdspCore.stations.size() and REdspCore.stations[j].id!=REdspCore.jncs[Log[i].item].stationId)j++;
						if(j<REdspCore.stations.size())
							msg=msg+REdspCore.stations[j].name;
						else
							msg=msg+"<i>unknown</i>";
						msg=msg+" ("+REdspCore.jncs[Log[i].item].name+")</a>";
					}
					else
						msg="<i>unknown</i> ("+Log[i].item+")";
					ret=ret+ST.GetString1("LogMessage"+Log[i].code, msg);
					break;
				case LogEntry.CODE_POST_CANCEL_USE:
				case LogEntry.CODE_POST_CANCEL_UNSEAL:
				case LogEntry.CODE_POST_CANCEL_SEAL:
				case LogEntry.CODE_POST_PERMISSION_ON:
				case LogEntry.CODE_POST_PERMISSION_OFF:
				case LogEntry.CODE_POST_TAKE_ON:
				case LogEntry.CODE_POST_TAKE_OFF:
					if(Log[i].item>=0 and Log[i].item<posts.size()){
						msg=posts[Log[i].item].name;
						if(posts[Log[i].item].stationIndex<REdspCore.stations.size())
							st="<a href=\"live://st/"+posts[Log[i].item].stationIndex+"\">"+REdspCore.stations[posts[Log[i].item].stationIndex].name+"</a>";
						else
							st="<i>unknown</i>";
					}
					else{
						msg="Post #"+Log[i].item;
						st="<i>unknown</i>";
					}
					ret=ret+ST.GetString2("LogMessage"+Log[i].code, msg, st);
					break;
				case LogEntry.CODE_SPAN_ADD_DEP_USE:
				case LogEntry.CODE_SPAN_ADD_DEP_UNSEAL:
				case LogEntry.CODE_SPAN_ADD_DEP_SEAL:
				case LogEntry.CODE_SPAN_ADD_APPR_USE:
				case LogEntry.CODE_SPAN_ADD_APPR_UNSEAL:
				case LogEntry.CODE_SPAN_ADD_APPR_SEAL:
					if (sigElement = inSignals.findElement(Log[i].item)) {
						sig = cast<zxSignal>Router.GetGameObject(sigElement.key);
					}
					if (sig) {
						msg = sig.privateName;
						int n = 0;
						while (n < REdspCore.stations.size() and REdspCore.stations[n].name != sig.stationName) ++n;
						if (n < REdspCore.stations.size())
							st = "<a href='live://st/" + n + "'>" + sig.stationName + "</a>";
						else
							st = "<i>" + sig.stationName + "</i>";
					}
					else {
						msg = "signal id: " + Log[i].item;
						st = "<i>unknown</i>";
					}
					ret = ret + ST.GetString2("LogMessage" + Log[i].code, msg, st);
					break;
				default:
					ret=ret+"code: "+Log[i].code+" item: "+Log[i].item+" info: "+Log[i].info;
					break;
			}
			ret=ret+"</td></tr>";
		}
		ret=ret+"</table>";
		LOGbrowser.LoadHTMLString(self, ret);
	}

	public void WriteLog(string user, int code, int item, string info){
		LogEntry Entry=new LogEntry();
		Entry.time=World.GetGameTime();
		Entry.user=user;
		Entry.code=code;
		Entry.item=item;
		Entry.info=info;
		Log[Log.size()]=Entry;
		UpdateLOGbrowser();
	}

	public int[] waitPosts=new int[0];
	public bool postsCheckerStarted=false;
	public thread void postsChecker(){
		if(postsCheckerStarted)return;
		postsCheckerStarted=true;
		while(waitPosts.size()>0){
			int i;
			for(i=waitPosts.size()-1;i>=0;i--){
				if(posts[waitPosts[i]].tryChangeState(junctionToken))
					waitPosts[i,i+1]=null;
			}
			Sleep(0.1);
		}
		postsCheckerStarted=false;
	}

	bool checkNoOutputRoute(zxSignal sig) {
		zx_DSP_JunctionInfo Jinfo = REdspCore.FindFirstJunctionInfo(sig, true, false, true);
		if (!Jinfo or !(Jinfo.state & zx_DSP_JunctionInfo.STATE_PATH))
			return true;
		int rcode = REdspCore.GetJunctionArrivalDirection(Jinfo.jnc, sig, false);
		if (Jinfo.pathdirection == zx_DSP_JunctionInfo.DIRECT_BACKWARD)
			return rcode != zx_DSP_JunctionInfo.JNC_BACK;
		if (Jinfo.pathdirection == zx_DSP_JunctionInfo.DIRECT_FORWARD) {
			if (rcode == zx_DSP_JunctionInfo.JNC_LEFT)
				return Jinfo.jnc.GetDirection() != JunctionBase.DIRECTION_LEFT;
			if (rcode == zx_DSP_JunctionInfo.JNC_RIGHT)
				return Jinfo.jnc.GetDirection() != JunctionBase.DIRECTION_RIGHT;
		}
		return false;
	}

	void spanFunction(BinarySortedElement sigElement, string function) {
		zxSignal sig = cast<zxSignal>Router.GetGameObject(sigElement.key);
		zxSignal sig2 = null;
		BinarySortedElement sigElement2 = null;
		if (function == "addapprsealb") {
			if ((sigElement.type & SPAN_TYPE) == SPAN_TYPE_AB) {
				sigElement.state = sigElement.state | SPAN_AB_STATE_SEAL_ADD_APPR;
			}
		}
		else if (function == "addapprsealr") {
			if ((sigElement.type & SPAN_TYPE) == SPAN_TYPE_AB) {
				sigElement.state = sigElement.state & ~SPAN_AB_STATE_SEAL_ADD_APPR;
			}
		}
		else if (function == "adddepsealb") {
			if ((sigElement.type & SPAN_TYPE) == SPAN_TYPE_AB) {
				sigElement.state = sigElement.state | SPAN_AB_STATE_SEAL_ADD_DEP;
			}
		}
		else if (function == "adddepsealr") {
			if ((sigElement.type & SPAN_TYPE) == SPAN_TYPE_AB) {
				sigElement.state = sigElement.state & ~SPAN_AB_STATE_SEAL_ADD_DEP;
			}
		}
		else {
			Soup span = sig.span_soup;
			if (span and span.GetNamedTagAsBool("Inited", false)) {
				sig2 = cast<zxSignal>Router.GetGameObject(span.GetNamedTag("end_sign"));
				if (sig2) {
					sigElement2 = inSignals.findElement(sig2.GetId());
				}
			}
			if (!sigElement2)
				return;
			if ((sigElement2.type & SPAN_TYPE) == SPAN_TYPE_AB) {
				ClearMessages("suDSPJunctionsTimer", "span" + sigElement.key);
				sigElement.state = sigElement.state & ~SPAN_AB_STATE_BUTTONS;
			}
			if (function == "dep") {
				if ((sigElement2.type & SPAN_TYPE) == SPAN_TYPE_AB) {
					if (sigElement2.type & SPAN_TYPE_NOCONFIRM or sigElement2.state & SPAN_AB_STATE_BTN_CONFIRM) {
						ClearMessages("suDSPJunctionsTimer", "span" + sigElement2.key);
						sigElement2.state = sigElement2.state & ~SPAN_AB_STATE_BUTTONS;
						if (checkNoOutputRoute(sig2))
							sig2.Switch_span(false);
					}
					else {
						sigElement.state = sigElement.state | SPAN_AB_STATE_BTN_DEP;
						PostMessage(me, "suDSPJunctionsTimer", "span" + sigElement.key, 30);
					}
				}
			}
			else if (function == "adddep") {
				if ((sigElement2.type & SPAN_TYPE) == SPAN_TYPE_AB and sigElement.state & SPAN_AB_STATE_SEAL_ADD_DEP) {
					if (sigElement2.state & SPAN_AB_STATE_BTN_ADD_APPR) {
						ClearMessages("suDSPJunctionsTimer", "span" + sigElement2.key);
						sigElement2.state = sigElement2.state & ~SPAN_AB_STATE_BUTTONS;
						if (checkNoOutputRoute(sig2))
							sig2.Switch_span(true);
					}
					else {
						sigElement.state = sigElement.state | SPAN_AB_STATE_BTN_ADD_DEP;
						PostMessage(me, "suDSPJunctionsTimer", "span" + sigElement.key, 30);
					}
				}
			}
			else if (function == "confirm") {
				if ((sigElement.type & SPAN_TYPE) == SPAN_TYPE_AB) {
					if (sigElement2.state & SPAN_AB_STATE_BTN_DEP) {
						ClearMessages("suDSPJunctionsTimer", "span" + sigElement2.key);
						sigElement2.state = sigElement2.state & ~SPAN_AB_STATE_BUTTONS;
						if (checkNoOutputRoute(sig))
							sig.Switch_span(false);
					}
					else {
						sigElement.state = sigElement.state | SPAN_AB_STATE_BTN_CONFIRM;
						PostMessage(me, "suDSPJunctionsTimer", "span" + sigElement.key, 30);
					}
				}
			}
			else if (function == "addappr") {
				if ((sigElement.type & SPAN_TYPE) == SPAN_TYPE_AB and sigElement.state & SPAN_AB_STATE_SEAL_ADD_APPR) {
					if (sigElement2.state & SPAN_AB_STATE_BTN_ADD_DEP) {
						ClearMessages("suDSPJunctionsTimer", "span" + sigElement2.key);
						sigElement2.state = sigElement2.state & ~SPAN_AB_STATE_BUTTONS;
						if (checkNoOutputRoute(sig))
							sig.Switch_span(true);
					}
					else {
						sigElement.state = sigElement.state | SPAN_AB_STATE_BTN_ADD_APPR;
						PostMessage(me, "suDSPJunctionsTimer", "span" + sigElement.key, 30);
					}
				}
			}
		}
		Soup sp = Constructors.NewSoup();
		sp.SetNamedTag("function", "spanstate");
		sp.SetNamedTag("sig1", sig.GetName());
		sp.SetNamedTag("state1", sigElement.state);
		if (sigElement2) {
			sp.SetNamedTag("sig2", sig2.GetName());
			sp.SetNamedTag("state2", sigElement2.state);
		}
		MultiplayerGame.BroadcastGameplayMessage("suDSPJunctions", "sync", sp);
		if (StationIndex >= 0 and StationIndex < REdspCore.stations.size() and (REdspCore.stations[StationIndex].name == sig.stationName or (sig2 and (REdspCore.stations[StationIndex].name == sig2.stationName))))
			UpdateDSPbrowser();
	}

	void onTimer(Message msg) {
		if (propertiesIsLoading) {
			return;
		}
		if (msg.minor[,4] = "span") {
			BinarySortedElement sigElement = inSignals.findElement(Str.ToInt(msg.minor[4,]));
			if (!sigElement)
				return;
			if ((sigElement.type & SPAN_TYPE) == SPAN_TYPE_AB) {
				sigElement.state = sigElement.state & ~SPAN_AB_STATE_BUTTONS;
				zxSignal sig = cast<zxSignal>Router.GetGameObject(sigElement.key);
				if (sig) {
					Soup sp = Constructors.NewSoup();
					sp.SetNamedTag("function", "spanstate");
					sp.SetNamedTag("sig1", sig.GetName());
					sp.SetNamedTag("state1", sigElement.state);
					MultiplayerGame.BroadcastGameplayMessage("suDSPJunctions", "sync", sp);
					if (StationIndex >= 0 and StationIndex < REdspCore.stations.size() and REdspCore.stations[StationIndex].name == sig.stationName)
						UpdateDSPbrowser();
				}
			}
		}
	}

	void Sync(Message msg){
		if (propertiesIsLoading) {
			return;
		}
		Soup sp=msg.paramSoup;
		string function=sp.GetNamedTag("function");
		if (function == "spanstate") {
			zxSignal sig;
			BinarySortedElement sigElement;
			if ((sig = cast<zxSignal>Router.GetGameObject(sp.GetNamedTag("sig1"))) and (sigElement = inSignals.findElement(sig.GetId()))) {
				sigElement.state = sp.GetNamedTagAsInt("state1", sigElement.state);
				if (StationIndex >= 0 and StationIndex < REdspCore.stations.size() and REdspCore.stations[StationIndex].name == sig.stationName)
					UpdateDSPbrowser();
			}
			if ((sig = cast<zxSignal>Router.GetGameObject(sp.GetNamedTag("sig2"))) and (sigElement = inSignals.findElement(sig.GetId()))) {
				sigElement.state = sp.GetNamedTagAsInt("state2", sigElement.state);
				if (StationIndex >= 0 and StationIndex < REdspCore.stations.size() and REdspCore.stations[StationIndex].name == sig.stationName)
					UpdateDSPbrowser();
			}
		}
		else if(function=="post"){
			int i=sp.GetNamedTagAsInt("post", -1);
			if(i>=0 and i<posts.size())
				posts[i].Sync(sp, junctionToken);
		}
		else if (function == "junctionpost") {
			int i = sp.GetNamedTagAsInt("post", -1);
			if (i >= 0 and i < posts.size() and posts[i].post)
				posts[i].post.Sync(sp);
		}
		else if (function == "span") {
			zxSignal sig = cast<zxSignal>Router.GetGameObject(sp.GetNamedTag("sig"));
			if (!sig)
				return;
			BinarySortedElement sigElement = inSignals.findElement(sig.GetId());
			if (!sigElement)
				return;
			spanFunction(sigElement, sp.GetNamedTag("subfunc"));
			int eventType = -1;
			if (function == "adddep") eventType = LogEntry.CODE_SPAN_ADD_DEP_USE;
			else if (function == "adddepsealb") eventType = LogEntry.CODE_SPAN_ADD_DEP_UNSEAL;
			else if (function == "adddepsealr") eventType = LogEntry.CODE_SPAN_ADD_DEP_SEAL;
			else if (function == "addappr") eventType = LogEntry.CODE_SPAN_ADD_APPR_USE;
			else if (function == "addapprsealb") eventType = LogEntry.CODE_SPAN_ADD_APPR_UNSEAL;
			else if (function == "addapprsealr") eventType = LogEntry.CODE_SPAN_ADD_APPR_SEAL;
			if (eventType >= 0)
				WriteLog(sp.GetNamedTag("__sender"), eventType, sigElement.key, "");
		}
	}

	thread void syncState(string client) {
		int i;
		JunctionPost[] processedPosts = new JunctionPost[0];
		for (i = 0; i < posts.size(); ++i) {
			posts[i].syncState(client);
			if (!posts[i].post)
				continue;
			int j = 0;
			while (j < processedPosts.size() and processedPosts[j] != posts[i].post) ++j;
			if (j >= processedPosts.size())
				continue;
			posts[i].post.syncState(client);
			processedPosts[processedPosts.size()] = posts[i].post;
		}
	}

	void SuDSPSync(Message msg) {
		if (propertiesIsLoading) {
			return;
		}
		Soup sp = msg.paramSoup;
		string function = sp.GetNamedTag("Action");
		if (function == "RequestControllerState") {
			syncState(sp.GetNamedTag("__sender"));
		}
	}

	void InitDSPbrowser(void){
		DSPbrowser = Constructors.NewBrowser();
		DSPbrowser.SetWindowRect(10,75,10+200,75+300); 
		DSPbrowser.SetWindowStyle(Browser.STYLE_DEFAULT);
		DSPbrowser.SetScrollEnabled(true);
		DSPbrowser.SetCloseEnabled(true);
		DSPbrowser.SetWindowTitle(ST.GetString("dsp-browser-title"));
//		DSPbrowser.SetButtonOverlayStyle(Browser.BS_OK, Browser.BS_None);
		DSPbrowser.SetRememberPosition(self, "dsp-browser");
		DSPbrowser.SetWindowGrow(300, 400, Interface.GetDisplayWidth(), Interface.GetDisplayHeight());
	}

	public void ChangeText(Message msg){
		if(msg.src==DSPbrowser){
			if(msg.minor=="live://refresh")
				UpdateDSPbrowser();
			else if(msg.minor[,18]=="live://setstation/"){
				int n=Str.ToInt(msg.minor[18,]);
				if(n>=0 and n<REdspCore.stations.size())
					StationIndex=n;
				UpdateDSPbrowser();
			}
			else if(msg.minor[,11]=="live://Post"){
				int n=Str.Find(msg.minor, "/", 11);
				if(n>0){
					int k=Str.ToInt(msg.minor[11,n]);
					if(k>=0 and k<posts.size())
						posts[k].OnChangeText(msg.minor[n+1,], junctionToken);
				}
			}
			else if (msg.minor[,11] == "live://span") {
				int n = Str.Find(msg.minor, "/", 11);
				if (n > 0) {
					BinarySortedElement sigElement = inSignals.findElement(Str.ToInt(msg.minor[11,n]));
					if (!sigElement)
						return;
					zxSignal sig = cast<zxSignal>Router.GetGameObject(sigElement.key);
					if (!sig)
						return;
					int st = 0;
					while (st < REdspCore.stations.size() and REdspCore.stations[st].name != sig.stationName) ++st;
					if (st >= REdspCore.stations.size())
						return;
					if (MultiplayerGame.IsActive() and !MultiplayerGame.IsServer() and !REdspCore.stations[st].availableControl)
						return;
					string func = msg.minor[n+1,];

					if (func == "addapprseal" or func == "adddepseal") {
						if ((
							(sigElement.state & SPAN_AB_STATE_SEAL_ADD_APPR and func == "addapprseal")
							or (sigElement.state & SPAN_AB_STATE_SEAL_ADD_DEP and func == "adddepseal")
						) and MultiplayerGame.IsActive() and !MultiplayerGame.IsServer())
							return;
						Menu menu = Constructors.NewMenu();
						menu.AddSeperator();
						if (func == "addapprseal") {
							if (sigElement.state & SPAN_AB_STATE_SEAL_ADD_APPR)
								menu.AddItem("Восстановить пломбу", me, "Browser-URL", "live://span" + sigElement.key + "/" + func + "r");
							else
								menu.AddItem("Сорвать пломбу", me, "Browser-URL", "live://span" + sigElement.key + "/" + func + "b");
						}
						if (func == "adddepseal") {
							if (sigElement.state & SPAN_AB_STATE_SEAL_ADD_DEP)
								menu.AddItem("Восстановить пломбу", me, "Browser-URL", "live://span" + sigElement.key + "/" + func + "r");
							else
								menu.AddItem("Сорвать пломбу", me, "Browser-URL", "live://span" + sigElement.key + "/" + func + "b");
						}
						DSPbrowser.PopupMenu(menu);
						return;
					}

					if (MultiplayerGame.IsActive() and !MultiplayerGame.IsServer()) {
						Soup sp = Constructors.NewSoup();
						sp.SetNamedTag("function", "span");
						sp.SetNamedTag("sig", sig.GetName());
						sp.SetNamedTag("subfunc", func);
						MultiplayerGame.SendGameplayMessageToServer("suDSPJunctions", "sync", sp);
					}
					else {
						spanFunction(sigElement, func);
						int eventType = -1;
						if (func == "adddep") eventType = LogEntry.CODE_SPAN_ADD_DEP_USE;
						else if (func == "adddepsealb") eventType = LogEntry.CODE_SPAN_ADD_DEP_UNSEAL;
						else if (func == "adddepsealr") eventType = LogEntry.CODE_SPAN_ADD_DEP_SEAL;
						else if (func == "addappr") eventType = LogEntry.CODE_SPAN_ADD_APPR_USE;
						else if (func == "addapprsealb") eventType = LogEntry.CODE_SPAN_ADD_APPR_UNSEAL;
						else if (func == "addapprsealr") eventType = LogEntry.CODE_SPAN_ADD_APPR_SEAL;
						if (eventType >= 0)
							WriteLog(REutility.GetUsername(), eventType, sigElement.key, "");
					}
				}
			}
		}
		else if(msg.src==LOGbrowser){
			if(msg.minor=="live://refresh")
				UpdateLOGbrowser();
			else if(msg.minor[,11]=="live://jnc/"){
				int n=Str.ToInt(msg.minor[11,]);
				if(!REdspCore)
					return;
				if(n<0 or n>=REdspCore.jncs.size())
					return;
			}
			else if(msg.minor[,10]=="live://st/"){
				int n=Str.ToInt(msg.minor[10,]);
				if(!REdspCore)
					return;
				if(n<0 or n>=REdspCore.stations.size())
					return;
				StationIndex=n;
				if(!DSPbrowser)
					InitDSPbrowser();
				UpdateDSPbrowser();
			}
		}
	}

	void BrowserClosed(Message msg){
		if(msg.src==DSPbrowser)
			DSPbrowser=null;
		else if(msg.src==LOGbrowser)
			LOGbrowser=null;
	}

	void InitLOGbrowser(void){
		LOGbrowser = Constructors.NewBrowser();
		LOGbrowser.SetWindowRect(10,75,10+500,75+300);
		LOGbrowser.SetWindowStyle(Browser.STYLE_DEFAULT);
		LOGbrowser.SetScrollEnabled(true);
		LOGbrowser.SetCloseEnabled(true);
		LOGbrowser.SetWindowTitle(ST.GetString("log-browser-title"));
//		LOGbrowser.SetButtonOverlayStyle(Browser.BS_OK, Browser.BS_None);
		LOGbrowser.SetRememberPosition(self, "log-browser");
		LOGbrowser.SetWindowGrow(500, 400, Interface.GetDisplayWidth(), Interface.GetDisplayHeight());
	}

	void GUIRequestMsgHdl(Message msg){
		if(msg.minor=="RequestDSPbrowser"){
			if (DSPbrowser){
				if (DSPbrowser.IsWindowMinimised())
					DSPbrowser.RestoreWindow();
			}
			else
				InitDSPbrowser();
			UpdateDSPbrowser();
			DSPbrowser.SetWindowVisible(true);
		}
		else if(msg.minor=="RequestLOGbrowser"){
			if (LOGbrowser){
				if (LOGbrowser.IsWindowMinimised())
					LOGbrowser.RestoreWindow();
			}
			else
				InitLOGbrowser();
			UpdateLOGbrowser();
			LOGbrowser.SetWindowVisible(true);
		}
	} /* end of GUIRequestMsgHdl */

	thread void RunInDriver(){
		if(World.GetCurrentModule()!=World.DRIVER_MODULE){
			return;
		}
		if(REdspLib and (REdspCore=cast<zx_DSP_Core>REdspLib.GetController())){
			while (propertiesIsLoading) {
				Sleep(2);
			}
//			REdspCore.SetExtension(me);
			REdspLib.AddMenuItem(me, "suDSPJunctionsRule", "RequestDSPbrowser", "su DSP Junctions - DSP");
			if(!MultiplayerGame.IsActive() or MultiplayerGame.IsServer())
				REdspLib.AddMenuItem(me, "suDSPJunctionsRule", "RequestLOGbrowser", "su DSP Junctions - LOG");

			int i = 0;
			ExternalControll[] faultPosts = new ExternalControll[0];
			while(i < posts.size()) {
				posts[i].REdspCore=REdspCore;
				if (!posts[i].checkIndexes()) {
					posts[i].tryFixIndexes();
					if (!posts[i].checkIndexes()) {
						faultPosts[faultPosts.size()] = posts[i];
						posts[i,i+1] = null;
						continue;
					}
				}
				++i;
			}
			for (i = 0; i < REdspCore.jncs.size(); ++i) {
				Sniff(REdspCore.jncs[i].jnc, "Object", "InnerEnter", true);
				Sniff(REdspCore.jncs[i].jnc, "MapObject", "View-Details", true);
			}
			suCore.AddExtraLink(me);
			AddHandler(me, "Browser-URL", "", "ChangeText");
			AddHandler(me, "Browser-Closed", "", "BrowserClosed");
			AddHandler(me, "Junction", "Toggled", "OnJunction");
			AddHandler(me, "suDSPJunctionsTimer", null, "onTimer");
			if (faultPosts.size() > 0) {
				string msg = "";
				for (i = 0; i < faultPosts.size(); ++i) {
					if (msg != "") {
						msg = msg + ", ";
					}
					msg = msg + faultPosts[i].name;
					if (faultPosts[i].stationIndex >= 0 and faultPosts[i].stationIndex < REdspCore.stations.size()) {
						msg = msg + " (" + REdspCore.stations[faultPosts[i].stationIndex].name + ")";
					}
				}
				Interface.Exception("faultPosts: " + msg);
			}
		}
	}

	void zxDSPEventHandler(Message msg) {
		if (msg.minor == "EndInit") {
			RunInDriver();
		}
	}

	public string GetDescriptionHTML(void){
		zx_DSP_Core REdspCore;
		int flag=1024;
		string ret;

		if(REdspLib and (REdspCore=cast<zx_DSP_Core>REdspLib.GetController())){
			ret=ret+"Stations:<br>";
			int i, j;
			for(i=0;i<REdspCore.stations.size();i++){
				if(i==StationIndex)
					ret=ret+"<a href='live://property/Station-1'>-";
				else
					ret=ret+"<a href='live://property/Station"+i+"'>+";
				zx_DSP_Station station = REdspCore.stations[i];
				ret = ret + "&nbsp;" + station.name + "</a><br>";

				if (i == StationIndex) {
					ret = ret + "<font color=\"#ff0000\">Функционал стрелок вынесен в пульт ДСП</font><br>";
				}

				for (j = 0; j < posts.size(); j++) {
					ExternalControll P = posts[j];
					if (P.stationIndex != i)
						continue;
					P.state = P.state | flag;
					if (i != StationIndex)
						continue;

					P.REdspCore = REdspCore;
					if (!P.checkIndexes()) {
						P.tryFixIndexes();
					}
//					P.stationIndex = i;
					ret = ret + "<br><a href='live://property/Post" + j + "/name'>" + P.name + "</a>&nbsp;<a href='live://property/RemovePost" + j + "'>X</a><br>";
					P.index = j;
					ret = ret + P.GetDescriptionHTML();

				}
				if (i == StationIndex)
					ret = ret + "<a href='live://property/AddPost" + i + "'>Добавить стрелочный пост</a><br><br>";
				for (j = 0; j < station.signals.size(); ++j) {
					zxSignal sig = station.signals[j];
					if (!(sig.Type & zxSignal.ST_IN))
						continue;
					BinarySortedElement sigElement = inSignals.findElement(sig.GetId());
					if (!sigElement) {
						sigElement = new BinarySortedElement();
						sigElement.key = sig.GetId();
						sigElement.type = SPAN_TYPE_AB;
						if (sig.wrong_dir)
							sigElement.state = SPAN_AB_STATE_DEP;
						else
							sigElement.state = 0;
						inSignals.addElement(sigElement);
					}
					sigElement.state = sigElement.state | flag;
					if (i != StationIndex)
						continue;
					BinarySortedElement sigElement2 = null;
					ret = ret + sig.privateName + " - ";
					Soup span = sig.span_soup;
					if (span and span.GetNamedTagAsBool("Inited", false)) {
						ret = ret + span.GetNamedTag("end_sign_s");
						zxSignal sig2 = cast<zxSignal>Router.GetGameObject(span.GetNamedTag("end_sign"));
						if (sig2) {
							sigElement2 = inSignals.findElement(sig2.GetId());
							if (!sigElement2) {
								sigElement2 = new BinarySortedElement();
								sigElement2.key = sig2.GetId();
								sigElement2.type = sigElement.type;
								sigElement2.state = flag;
								if ((sigElement.type & SPAN_TYPE) == SPAN_TYPE_AB and sig2.wrong_dir)
									sigElement2.type = sigElement2.type | SPAN_AB_STATE_DEP;
								inSignals.addElement(sigElement2);
							}
						}
						else {
							ret = ret + " <font color=\"#ff0000\">" + span.GetNamedTag("end_sign") + " not found</font>";
						}
					}
					else
						ret = ret + "<i>???</i>";
					ret = ret + "<br>";
					if (!sigElement2)
						continue;
					int type1 = sigElement.type & SPAN_TYPE;
					int type2 = sigElement2.type & SPAN_TYPE;

					ret = ret + "<a href=\"live://property/SpanType^" + sigElement.key + "^" + sigElement2.key + "^0\">" + HTMLWindow.RadioButton("", type1 == SPAN_TYPE_NONE and type2 == SPAN_TYPE_NONE) + " <i>нет</i></a>&nbsp;&nbsp;";
					ret = ret + "<a href=\"live://property/SpanType^" + sigElement.key + "^" + sigElement2.key + "^1\">" + HTMLWindow.RadioButton("", type1 == SPAN_TYPE_TOKEN or (type1 == SPAN_TYPE_NONE and type2 == SPAN_TYPE_TOKEN)) + " ЭЖС</a>&nbsp;&nbsp;";
					ret = ret + "<a href=\"live://property/SpanType^" + sigElement.key + "^" + sigElement2.key + "^2\">" + HTMLWindow.RadioButton("", type1 == SPAN_TYPE_PAB or (type1 == SPAN_TYPE_NONE and type2 == SPAN_TYPE_PAB)) + " ПАБ</a>&nbsp;&nbsp;";
					ret = ret + "<a href=\"live://property/SpanType^" + sigElement.key + "^" + sigElement2.key + "^3\">" + HTMLWindow.RadioButton("", type1 == SPAN_TYPE_AB or (type1 == SPAN_TYPE_NONE and type2 == SPAN_TYPE_AB)) + " АБ</a>";
					if (type1 or type2) {
						ret = ret + "&nbsp;&nbsp;|&nbsp;&nbsp;";
						ret = ret + "<a href=\"live://property/SpanDir^" + sigElement.key + "^" + sigElement2.key + "^in\">" + HTMLWindow.RadioButton("", type2 == SPAN_TYPE_NONE) + " &lt;&lt;-</a>&nbsp;&nbsp;";
						ret = ret + "<a href=\"live://property/SpanDir^" + sigElement.key + "^" + sigElement2.key + "^both\">" + HTMLWindow.RadioButton("", type1 == type2) + " &lt;-&gt;</a>&nbsp;&nbsp;";
						ret = ret + "<a href=\"live://property/SpanDir^" + sigElement.key + "^" + sigElement2.key + "^out\">" + HTMLWindow.RadioButton("", type1 == SPAN_TYPE_NONE) + " -&gt;&gt;</a> ";
					}
					ret = ret + "<br>";
					if (type1 == SPAN_TYPE_PAB or type1 == SPAN_TYPE_AB) {
						ret = ret + "<a href=\"live://property/SpanNoConfirm^" + sigElement.key + "^" + sigElement2.key + "\">" + HTMLWindow.CheckBox("", sigElement.type & SPAN_TYPE_NOCONFIRM) + " SPAN_TYPE_NOCONFIRM</a><br>";
					}
				}
				ret=ret+"<br>";
			}
		}
		else{
			ret=ret+"DSP controller not found<br>unsorted list:<br>";
		}

		int i;
		for (i = 0; i < posts.size(); ++i) {
			ExternalControll P = posts[i];
			if (P.state & flag)
				P.state = P.state & ~flag;
			else {
				P.REdspCore = REdspCore;
				if (!P.checkIndexes()) {
					P.tryFixIndexes();
				}
				ret = ret + "<br><a href='live://property/Post" + i + "/name'>" + P.name + "</a>&nbsp;<a href='live://property/RemovePost" + i + "'>X</a><br>";
				P.index = i;
				ret = ret + P.GetDescriptionHTML();
			}
		}
		for (i = 0; i < inSignals.count(); ++i) {
			BinarySortedElement sigElement = inSignals.getElementByIndex(i);
			zxSignal sig = cast<zxSignal>Router.GetGameObject(sigElement.key);
			if (!sig)
				continue;
			if (sigElement.state & flag)
				sigElement.state = sigElement.state & ~flag;
			else
				ret = ret + " " + sig.privateName + " @ " + sig.stationName + " (" + sig.GetName() + ") <a href=\"live://property/DeleteInSig" + i + "\">X</a><br>";
		}

		return ret;
	}

	string GetPropertyType(string id){
		if(id[,7]=="Station")return "link";
		if(id[,4]=="junc")return "link";
		if(id[,7]=="AddPost")return "link";
		if(id[,10]=="RemovePost")return "link";
		if(id[,4]=="Post"){
			int n=Str.Find(id, "/", 4);
			if (n > 0) {
				int k = Str.ToInt(id[4,n]);
				if (k >= 0 and k < posts.size()) {
					return posts[k].GetPropertyType(id[n+1,]);
				}
			}
			return "link";
		}
		if (id[,8] == "SpanType") return "link";
		if (id[,7] == "SpanDir") return "link";
		if (id[,13] == "SpanNoConfirm") return "link";
		if (id[,11] == "DeleteInSig") return "link";
		return inherited(id);
	}

	public string GetPropertyValue(string id){
		if(id[,4]=="Post"){
			int n=Str.Find(id, "/", 4);
			if(n>0){
				int k=Str.ToInt(id[4,n]);
				if(k>=0 and k<posts.size()){
					return posts[k].GetPropertyValue(id[n+1,]);
				}
			}
		}
		return inherited(id);
	}

	void SetPropertyValue(string id, string value){
		if(id[,4]=="Post"){
			int n=Str.Find(id, "/", 4);
			if(n>0){
				int k=Str.ToInt(id[4,n]);
				if(k>=0 and k<posts.size()){
					posts[k].SetPropertyValue(id[n+1,], value);
				}
			}
		}
		else inherited(id, value);
	}

	void SetPropertyValue(string id, int value){
		if(id[,4]=="Post"){
			int n=Str.Find(id, "/", 4);
			if(n>0){
				int k=Str.ToInt(id[4,n]);
				if(k>=0 and k<posts.size()){
					posts[k].SetPropertyValue(id[n+1,], value);
				}
			}
		}
		else inherited(id, value);
	}

	public string[] GetPropertyElementList(string id){
		if(id[,4]=="Post"){
			int n=Str.Find(id, "/", 4);
			if(n>0){
				int k=Str.ToInt(id[4,n]);
				if(k>=0 and k<posts.size()){
					return posts[k].GetPropertyElementList(id[n+1,]);
				}
			}
		}
		return inherited(id);
	}

	void SetPropertyValue(string id, string value, int index){
		if(id[,4]=="Post"){
			int n=Str.Find(id, "/", 4);
			if(n>0){
				int k=Str.ToInt(id[4,n]);
				if(k>=0 and k<posts.size()){
					posts[k].SetPropertyValue(id[n+1,], value, index);
				}
			}
		}
		else inherited(id, value, index);
	}

	void LinkPropertyValue(string id){
		if(id[,7]=="Station")
			StationIndex=Str.ToInt(id[7,]);
		else if(id[,7]=="AddPost"){
			int n=posts.size();
			posts[n]=new ExternalControll();
			posts[n].rule=me;
			posts[n].stationIndex = Str.ToInt(id[7,]);
			posts[n].name="Post "+n;
			posts[n].index = n;
		}
		else if(id[,10]=="RemovePost"){
			int n=Str.ToInt(id[10,]);
			if(n >= 0 and n < posts.size()) {
				posts[n,n+1] = null;
				int i;
				for (i = 0; i < posts.size(); ++i) {
					posts[i].index = i;
				}
			}
		}
		else if(id[,4]=="Post"){
			int n=Str.Find(id, "/", 4);
			if(n>0){
				int k=Str.ToInt(id[4,n]);
				if(k>=0 and k<posts.size()){
					posts[k].LinkPropertyValue(id[n+1,]);
				}
			}
		}
		else if (id[,8] == "SpanType") {
			string[] indexes = Str.Tokens(id[9,], "^");
			if (indexes.size() < 3)
				return;
			BinarySortedElement sigElement1 = inSignals.findElement(Str.ToInt(indexes[0]));
			BinarySortedElement sigElement2 = inSignals.findElement(Str.ToInt(indexes[1]));
			if (!sigElement1 or !sigElement2)
				return;
			int oldType1 = sigElement1.type & SPAN_TYPE;
			int oldType2 = sigElement2.type & SPAN_TYPE;
			int newType = Str.ToInt(indexes[2]) & SPAN_TYPE;
			sigElement1.type = sigElement1.type & ~SPAN_TYPE;
			sigElement2.type = sigElement2.type & ~SPAN_TYPE;
			if (oldType1 == oldType2) {
				sigElement1.type = sigElement1.type | newType;
				sigElement2.type = sigElement2.type | newType;
			}
			else if (oldType1 == SPAN_TYPE_NONE) {
				sigElement2.type = sigElement2.type | newType;
			}
			else {
				sigElement1.type = sigElement1.type | newType;
			}
		}
		else if (id[,7] == "SpanDir") {
			string[] indexes = Str.Tokens(id[8,], "^");
			if (indexes.size() < 3)
				return;
			BinarySortedElement sigElement1 = inSignals.findElement(Str.ToInt(indexes[0]));
			BinarySortedElement sigElement2 = inSignals.findElement(Str.ToInt(indexes[1]));
			if (!sigElement1 or !sigElement2)
				return;

			int oldType1 = sigElement1.type & SPAN_TYPE;
			int oldType2 = sigElement2.type & SPAN_TYPE;
			int newType = oldType1;
			if (oldType1 == SPAN_TYPE_NONE)
				newType = oldType2;

			sigElement1.type = sigElement1.type & ~SPAN_TYPE;
			sigElement2.type = sigElement2.type & ~SPAN_TYPE;
			if (indexes[2] == "both" or indexes[2] == "in")
				sigElement1.type = sigElement1.type | newType;
			if (indexes[2] == "both" or indexes[2] == "out")
				sigElement2.type = sigElement2.type | newType;
		}
		else if (id[,13] == "SpanNoConfirm") {
			string[] indexes = Str.Tokens(id[14,], "^");
			if (indexes.size() < 1)
				return;
			BinarySortedElement sigElement = inSignals.findElement(Str.ToInt(indexes[0]));
			if (!sigElement)
				return;
			sigElement.type = sigElement.type ^ SPAN_TYPE_NOCONFIRM;
			if ((sigElement.type & SPAN_TYPE) == SPAN_TYPE_AB and indexes.size() > 1) {
				BinarySortedElement sigElement2 = inSignals.findElement(Str.ToInt(indexes[1]));
				if (!sigElement2)
					return;
				if (sigElement.type & SPAN_TYPE_NOCONFIRM)
					sigElement2.type = sigElement2.type | SPAN_TYPE_NOCONFIRM;
				else
					sigElement2.type = sigElement2.type & ~SPAN_TYPE_NOCONFIRM;
			}
		}
		else if (id[,11] == "DeleteInSig") {
			inSignals.DeleteElementByNmb(Str.ToInt(id[11,]));
		}
		else inherited(id);
	}

	public Soup GetProperties(void){
		Soup soup = inherited();
		int i;
		soup.SetNamedTag("postcount", posts.size());
		for(i=0;i<posts.size();i++)
			soup.SetNamedSoup("post."+i, posts[i].GetProperties());
		soup.SetNamedTag("insigcount", inSignals.count());
		for (i = 0; i < inSignals.count(); ++i) {
			BinarySortedElement sigElement = inSignals.getElementByIndex(i);
			Signal sig = cast<Signal>Router.GetGameObject(sigElement.key);
			if (!sig)
				continue;
			soup.SetNamedTag("insigname." + i, sig.GetName());
			soup.SetNamedTag("insigtype." + i, sigElement.type);
			soup.SetNamedTag("insigstate." + i, sigElement.state);
		}
		return soup;
	}

	thread void asyncSetProperties(Soup soup) {
		propertiesIsLoading = true;
		int sleepCounter = 0;
		int i, n;
		n=soup.GetNamedTagAsInt("postcount");
		posts=new ExternalControll[n];
		for(i=0;i<n;i++){
			posts[i]=new ExternalControll();
			posts[i].rule=me;
			posts[i].index=i;
//			posts[i].REdspLib=REdspLib;
			posts[i].SetProperties(soup.GetNamedSoup("post."+i));
			if (++sleepCounter > 100) {
				sleepCounter = 0;
				Sleep(0.001);
			}
		}
		inSignals.Clear();
		n = soup.GetNamedTagAsInt("insigcount");
		for (i = 0; i < n; ++i) {
			Signal sig = cast<Signal>Router.GetGameObject(soup.GetNamedTag("insigname." + i));
			if (!sig)
				continue;
			BinarySortedElement sigElement = new BinarySortedElement();
			sigElement.key = sig.GetId();
			sigElement.type = soup.GetNamedTagAsInt("insigtype." + i);
			sigElement.state = soup.GetNamedTagAsInt("insigstate." + i);
			inSignals.addElement(sigElement);
			if (++sleepCounter > 100) {
				sleepCounter = 0;
				Sleep(0.001);
			}
		}
		propertiesIsLoading = false;
	}

	public void SetProperties(Soup soup) {
		inherited(soup);
		Soup tmp = Constructors.NewSoup();
		tmp.Copy(soup);
		asyncSetProperties(tmp);
		if (REdspLib and (REdspCore=cast<zx_DSP_Core>REdspLib.GetController())) {
			AddHandler(REdspCore, "ZXDSP-Event", null, "zxDSPEventHandler");
		}
	}

	void OnJunction(Message msg){
		if (propertiesIsLoading) {
			return;
		}
		JunctionBase JM = cast<JunctionBase>msg.src;
		if (!JM)
			return;
		zx_DSP_JunctionInfo Jinfo = REdspCore.FindJunctionInfo(cast<Junction>JM, false, true);
		if (!Jinfo)
			return;
		if (!MultiplayerGame.IsActive() or MultiplayerGame.IsServer()) {
			if (Jinfo.state & zx_DSP_JunctionInfo.STATE_LOCALCONTROL) {
				if (Jinfo.pathIndex >=0 and Jinfo.pathIndex < posts.size()) {
					posts[Jinfo.pathIndex].onJunction(JM);
				}
			}
//			REdspCore.RaiseUpdateJunctionInfo(Jinfo.index);
		}
		if (StationIndex >= 0 and StationIndex < REdspCore.stations.size() and Jinfo.stationId == REdspCore.stations[StationIndex].id)
			UpdateDSPbrowser();
	}

	public void UpdateSignalState(zxSignal zxsign, int NewState, int priority) {
		if (
			zxsign.Type & zxSignal.ST_UNTYPED
			and zxsign.Type & (zxSignal.ST_PERMOPENED | zxSignal.ST_IN)
			and StationIndex >= 0
			and StationIndex < REdspCore.stations.size()
			and (REdspCore.stations[StationIndex].name == zxsign.stationName or (
				zxsign.span_soup
				and REdspCore.stations[StationIndex].name == zxsign.span_soup.GetNamedTag("end_sign_s")
			))
		) {
			UpdateDSPbrowser();
		}
	}

	public void UpdateSignalSpanDirection(zxSignal zxsign) {
		BinarySortedElement sigElement = inSignals.findElement(zxsign.GetId());
		BinarySortedElement sigElement2 = null;
		zxSignal sig2 = null;
		Soup span = zxsign.span_soup;
		if (span and span.GetNamedTagAsBool("Inited", false)) {
			sig2 = cast<zxSignal>Router.GetGameObject(span.GetNamedTag("end_sign"));
			if (sig2) {
				sigElement2 = inSignals.findElement(sig2.GetId());
			}
		}

		if (sigElement2) {
			int type2 = sigElement2.type & SPAN_TYPE;
			if (type2 == SPAN_TYPE_AB) {
				sigElement2.state = sigElement2.state | SPAN_AB_STATE_DEP;
			}
		}

		if (sigElement) {
			int type = sigElement.type & SPAN_TYPE;
			if (type == SPAN_TYPE_AB) {
				sigElement.state = sigElement.state & ~SPAN_AB_STATE_DEP;
			}
		}
		UpdateDSPbrowser();
	}

	public void Init(Asset asset){
		self=asset;
		inherited(self);
//		Interface.Print("HandJunctions::Init");
		inSignals = new BinarySortedArray();
/* 		KUID suDSPJunctionsLibKUID = self.LookupKUIDTable("sudspjunctionslib");
		suDSPJunctionsLib = World.GetLibrary(suDSPJunctionsLibKUID); */
		suCore = cast<zxLibruary_core>TrainzScript.GetLibrary(self.LookupKUIDTable("su-core"));
		mpsessionlib = cast<MultiplayerSessionManager>TrainzScript.GetLibrary(self.LookupKUIDTable("multiplayer-core"));
		REutility = cast<UtilityLibrary>TrainzScript.GetLibrary(self.LookupKUIDTable("reutility"));
		REdspLib = cast<zx_DSP_Library>TrainzScript.GetLibrary(self.LookupKUIDTable("REdspLib"));
		if (REdspLib and (REdspCore=cast<zx_DSP_Core>REdspLib.GetController())) {
			AddHandler(REdspCore, "ZXDSP-Event", null, "zxDSPEventHandler");
		}
		ST=self.GetStringTable();
		AddHandler(me, "suDSPJunctionsRule", "", "GUIRequestMsgHdl");
		AddHandler(me, "suDSPjunctions", "sync", "Sync");
		AddHandler(me, "ZXDSP_Sync", "CommandQuery", "SuDSPSync");
		string[] rights = new string[1];
		rights[0] = "junction-dir";
		junctionToken = IssueSecurityToken(asset.GetKUID(), rights);
	}
};