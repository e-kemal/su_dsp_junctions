﻿
kuid                                    <kuid2:216981:60021:8>
kind                                    "behavior"
username                                "sU_DSP_junctions"
category-class                          "YR"
category-region                         "BY;RU;SU;UA"
category-era                            "1960s;1970s;1980s;1990s;2000s;2010s"
icon-texture                            "icon.texture"
script                                  "sudspjunctions.gs"
class                                   "suDSPJunctions"
trainz-build                            4.5
license                                 

thumbnails
{
  0
  {
    image                               "thumbnail.jpg"
    width                               240
    height                              180
  }
}

string-table
{
  description                           "Правило для ручного переключения ручных стрелок"
  jnc-browser-title                     "Стрелка"
  tcm-browser-title                     "Стрелки - ТЧМ"
  dsp-browser-title                     "Стрелки - ДСП"
  log-browser-title                     "Стрелки - журнал"
  DisplayAtStart                        "отображать окно при старте сессии"
  LogMessage0                           "перевод ручной стрелки $0"
  LogMessage1                           "нажатие кнопки вспомогательного перевода стрелки $0"
  LogMessage2                           "срыв пломбы с кнопки вспомогательного перевода $0"
  LogMessage3                           "восстановление пломбы кнопки вспомогательного перевода $0"
  LogMessage4                           "взрез стрелки $0"
  LogMessage5                           "восстановление стрелки $0"
  LogMessage6                           "нажатие кнопки отмены манёвров на посту $0 станции $1"
  LogMessage7                           "срыв пломбы с кнопки отмены манёвров на посту $0 станции $1"
  LogMessage8                           "восстановление пломбы кнопки отмены манёвров на посту $0 станции $1"
  LogMessage9                           "разрешение манёвров на посту $0 станции $1"
  LogMessage10                          "отмена разрешения манёвров на посту $0 станции $1"
  LogMessage11                          "подтверждение восприятия разрешения манёвров на посту $0 станции $1"
  LogMessage12                          "отмена восприятия разрешения манёвров на посту $0 станции $1"
  LogMessage13                          "перевод стрелки $0 с поста местного управления"
  LogMessage14                          "нажатие на кнопку вспомогательного отправления светофора $0 станции $1"
  LogMessage15                          "срыв пломбы вспомогательного отправления светофора $0 станции $1"
  LogMessage16                          "восстановление пломбы вспомогательного отправления светофора $0 станции $1"
  LogMessage17                          "нажатие на кнопку вспомогательного приёма светофора $0 станции $1"
  LogMessage18                          "срыв пломбы вспомогательного приёма светофора $0 станции $1"
  LogMessage19                          "восстановление пломбы вспомогательного приёма светофора $0 станции $1"
}

script-include-table
{
  0                                     <kuid2:151055:60038:5>
  1                                     <kuid2:151055:60027:9>
  2                                     <kuid2:151055:28008:18>
  3                                     <kuid:30501:1020>
}

kuid-table
{
  REutility                             <kuid2:151055:60038:5>
  REdspLib                              <kuid2:151055:60027:9>
  su-core                               <kuid2:400260:100120:56>
  2                                     <kuid2:151055:28008:18>
  multiplayer-core                      <kuid:30501:1020>
}