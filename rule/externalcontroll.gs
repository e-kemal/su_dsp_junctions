include "zx_dsp_core.gs"
include "sudspjunctions.gs"
include "junctionpost.gs"

class ExternalControll{
	public suDSPJunctions rule;
//	public zx_DSP_Library REdspLib;
	public zx_DSP_Core REdspCore;
	public int index=-1;
//	public int stationId;
	public int stationIndex;
	public string name;
	Junction[] innerProtective = new Junction[0];
	int[] innerProtectiveIndex = new int[0];
	int[] innerProtectiveDir = new int[0];
	Junction[] outerProtective = new Junction[0];
	int[] outerProtectiveIndex = new int[0];
	int[] outerProtectiveDir = new int[0];
	zxSignal[] signals=new zxSignal[0];
	public int state;
	public define int POST_STATE_DSP		= 1;
	public define int POST_STATE_EXTERNAL	= 2;
	public define int POST_STATE_FACT		= 4;
	public define int POST_STATE_SEAL		= 8;
	public JunctionPost post;
	int postIndex;

	bool checkSignal(zxSignal sig)
	{
		bool f = true;
//Interface.Print("begin check signal: "+sig.privateName);
		GSTrackSearch ts = sig.BeginTrackSearch(true);
		while(ts.SearchNextObject()){
			object obj=ts.GetObject();
			if(ts.GetFacingRelativeToSearchDirection() and cast<zxSignal>obj){//открываемся до любого попутного светофора
				f=true;
//Interface.Print("front sign found "+(cast<zxSignal>obj).privateName);
				break;
			}
			JunctionBase JM=cast<JunctionBase>obj;
			if(JM){
				zx_DSP_JunctionInfo Jinfo=REdspCore.FindJunctionInfo(cast<Junction>JM, false, true);
				if (Jinfo.type & zx_DSP_JunctionInfo.JNC_MANUAL){//в нецентрализованную зону - открываемся
					f=true;
//Interface.Print("hand");
					break;
				}
				if ((Jinfo.state & zx_DSP_JunctionInfo.STATE_LOCALCONTROL) == 0 or Jinfo.pathIndex != index) { //Если стрелка не наша, то не открываемся
					f=false;
//Interface.Print("out");
					break;
				}
				f=false;
			}
			if(cast<zxSignal>obj){//если встретили обратный светофор, то, наверно, откроемся, в надежде, что это нецентрализованная зона или тупик
				int i = 0;
				while (i < signals.size() and signals[i] != obj) ++i;
				if (i >= signals.size()) {//стрелку ограждает не наш светофор - не будем открываться
					f = false;
					break;
				}
				f = true;
//Interface.Print("backsign");
			}
		}
		return f;
	}

	public void onJunction(Junction JM) {
		if (!(state & POST_STATE_DSP))
			return;
		if (!(state & POST_STATE_EXTERNAL))
			return;
		if (!(state & POST_STATE_FACT))
			return;
		int i;
		for (i = 0; i < signals.size(); ++i) {
			if (checkSignal(signals[i])){
				int oldMainState = signals[i].MainState;
				zxRouter MU = (cast<zxSignal_main>signals[i]).MU;
				signals[i].MainState = 20;
				if (MU and oldMainState != 1 and oldMainState != 19) {
					MU.UpdateMU();
				}
			}
			else {
				if(signals[i].ex_sgn[1])
					signals[i].MainState = 1;
				else
					signals[i].MainState = 19;
			}
			signals[i].SetSignal(true);
		}
	}

	void ChangeState(SecurityToken token, bool newState){
		int i;
		if(!newState){
			for(i=0;i<signals.size();i++){
				if(signals[i].ex_sgn[1])
					signals[i].MainState=1;
				else
					signals[i].MainState=19;
				signals[i].SetSignal(true);
				signals[i].Type=signals[i].Type|zxSignal.ST_UNTYPED;
			}
		}
		if (post) {
			int mask = 1;
			for (i = 0; i < postIndex; i++) mask = mask * 2;
			if (newState and (state & POST_STATE_EXTERNAL))
				post.permissions = post.permissions | mask;
			else
				post.permissions = post.permissions & ~mask;
			for (i = 0; i < post.junctions.size(); i++) {
				if (!(post.jncPermissions[i] & mask))
					continue;//Стрелка входит в этот пост, но не входит в этот вариант
				zx_DSP_JunctionInfo Jinfo = REdspCore.jncs[post.jncIndexes[i]];
				if(newState){
					Jinfo.state = Jinfo.state | zx_DSP_JunctionInfo.STATE_PATHLOCK | zx_DSP_JunctionInfo.STATE_SHUNTLOCK | zx_DSP_JunctionInfo.STATE_MANUALLOCK | zx_DSP_JunctionInfo.STATE_LOCALCONTROL;
					Jinfo.pathIndex=index;
					post.jPos(i);
					REdspCore.SyncJunctionInfo(Jinfo.index);
					continue;
				}
				Jinfo.pathIndex = -1;
				Jinfo.state = Jinfo.state & ~zx_DSP_JunctionInfo.STATE_LOCALCONTROL;
				if ((Jinfo.state & zx_DSP_JunctionInfo.STATE_CUTUP) == 0) {//если есть взрез, то блокировки не снимаем
					Jinfo.state = Jinfo.state & ~(zx_DSP_JunctionInfo.STATE_PATHLOCK | zx_DSP_JunctionInfo.STATE_SHUNTLOCK);
					Jinfo.state = Jinfo.state & ~(zx_DSP_JunctionInfo.STATE_MANUALLOCK);
				}
				REdspCore.SyncJunctionInfo(Jinfo.index);

			}
		}
		for(i=0;i<innerProtective.size();i++){
			zx_DSP_JunctionInfo Jinfo = REdspCore.jncs[innerProtectiveIndex[i]];
			if(newState){
				Jinfo.state = Jinfo.state | zx_DSP_JunctionInfo.STATE_PATHLOCK | zx_DSP_JunctionInfo.STATE_SHUNTLOCK | zx_DSP_JunctionInfo.STATE_MANUALLOCK | zx_DSP_JunctionInfo.STATE_LOCALCONTROL;
				Jinfo.pathIndex=index;
				Jinfo.SetDirection(token, innerProtectiveDir[i]);
				REdspCore.SyncJunctionInfo(Jinfo.index);
				continue;
			}
			Jinfo.pathIndex = -1;
			Jinfo.state = Jinfo.state & ~zx_DSP_JunctionInfo.STATE_LOCALCONTROL;
			if ((Jinfo.state & zx_DSP_JunctionInfo.STATE_CUTUP) == 0) {//если есть взрез, то блокировки не снимаем
				Jinfo.state = Jinfo.state & ~(zx_DSP_JunctionInfo.STATE_PATHLOCK | zx_DSP_JunctionInfo.STATE_SHUNTLOCK);
				Jinfo.state = Jinfo.state & ~(zx_DSP_JunctionInfo.STATE_MANUALLOCK);
			}
			REdspCore.SyncJunctionInfo(Jinfo.index);

		}
		for(i=0;i<outerProtective.size();i++){
			zx_DSP_JunctionInfo Jinfo = REdspCore.jncs[outerProtectiveIndex[i]];
			if(newState){
				Jinfo.state=Jinfo.state|zx_DSP_JunctionInfo.STATE_MANUALLOCK;
				Jinfo.SetDirection(token, outerProtectiveDir[i]);
				REdspCore.SyncJunctionInfo(Jinfo.index);
				continue;
			}
			if ((Jinfo.state & zx_DSP_JunctionInfo.STATE_CUTUP) == 0) {//если есть взрез, то блокировки не снимаем
				Jinfo.state = Jinfo.state & ~(zx_DSP_JunctionInfo.STATE_MANUALLOCK);
				REdspCore.SyncJunctionInfo(Jinfo.index);
			}

		}
		if(newState){
			for(i=0;i<signals.size();i++){
				signals[i].Type=signals[i].Type&~zxSignal.ST_UNTYPED;

				if(checkSignal(signals[i])){
					signals[i].MainState=20;
					signals[i].SetSignal(true);
				}
			}
		}

		if(newState)
			state=state|POST_STATE_FACT;
		else
			state=state&~POST_STATE_FACT;
		Soup sp=Constructors.NewSoup();
		sp.SetNamedTag("function", "post");
		sp.SetNamedTag("post", index);
		sp.SetNamedTag("subfunc", "state");
		sp.SetNamedTag("state", state);
		if (post)
			sp.SetNamedTag("post.permissions", post.permissions);
		MultiplayerGame.BroadcastGameplayMessage("suDSPjunctions", "sync", sp);
		if(rule.StationIndex==stationIndex)
			rule.UpdateDSPbrowser();
		if (post and post.browser)
			post.UpdateBrowser();
	}

	public bool tryChangeState(SecurityToken token){
		if(((state&POST_STATE_DSP)!=0)!=((state&POST_STATE_EXTERNAL)!=0))
			return true;//кнопки в разном положении. когда нажмут - тогда и запустим проверку
		if(((state&POST_STATE_DSP)!=0)==((state&POST_STATE_FACT)!=0))
			return true;//менять состояние не требуется
		int i;
		if (post) {
			int mask = 1;
			for (i = 0; i < postIndex; i++) mask = mask * 2;
			for (i = 0; i < post.junctions.size(); i++) {
				if (!(post.jncPermissions[i] & mask))
					continue;//Стрелка входит в этот пост, но не входит в этот вариант
				zx_DSP_JunctionInfo Jinfo = REdspCore.jncs[post.jncIndexes[i]];
				if (Jinfo.masterJunction) {
					Jinfo = Jinfo.masterJunction;
				}
				if(Jinfo.state&(zx_DSP_JunctionInfo.STATE_PATH|zx_DSP_JunctionInfo.STATE_SHUNT|zx_DSP_JunctionInfo.STATE_TRAIN|zx_DSP_JunctionInfo.STATE_LOCK))
					return false;//стрелка в маршруте
				if (Jinfo.type & zx_DSP_JunctionInfo.JNC_MANUAL)
					return false;//нет контроля положения стрелки
				if (Jinfo.state & zx_DSP_JunctionInfo.STATE_CUTUP)
					return false;//нет контроля положения стрелки
				if ((Jinfo.state & zx_DSP_JunctionInfo.STATE_LOCALCONTROL) and Jinfo.pathIndex != index and (state & POST_STATE_DSP))
					return false;//стрелка передана на другой пост
				if (Jinfo.slaveJunctions) {
					int j;
					int n = Jinfo.slaveJunctions.size();
					for (j = 0; j < n; ++j) {
						if (Jinfo.slaveJunctions[j].state & (zx_DSP_JunctionInfo.STATE_PATH | zx_DSP_JunctionInfo.STATE_SHUNT | zx_DSP_JunctionInfo.STATE_TRAIN | zx_DSP_JunctionInfo.STATE_LOCK)) {
							return false;//стрелка в маршруте
						}
						if (Jinfo.type & zx_DSP_JunctionInfo.JNC_MANUAL) {
							return false;//нет контроля положения стрелки
						}
						if (Jinfo.state & zx_DSP_JunctionInfo.STATE_CUTUP) {
							return false;//нет контроля положения стрелки
						}
						if ((Jinfo.slaveJunctions[j].state & zx_DSP_JunctionInfo.STATE_LOCALCONTROL) and Jinfo.slaveJunctions[j].pathIndex != index and (state & POST_STATE_DSP)) {
							return false;//стрелка передана на другой пост
						}
					}
				}
			}
		}
		for(i=0;i<innerProtective.size();i++){
			zx_DSP_JunctionInfo Jinfo = REdspCore.jncs[innerProtectiveIndex[i]];
			if (Jinfo.masterJunction) {
				Jinfo = Jinfo.masterJunction;
			}
			if(Jinfo.state&(zx_DSP_JunctionInfo.STATE_PATH|zx_DSP_JunctionInfo.STATE_SHUNT|zx_DSP_JunctionInfo.STATE_TRAIN|zx_DSP_JunctionInfo.STATE_LOCK))
				return false;//стрелка в маршруте
			if (Jinfo.type & zx_DSP_JunctionInfo.JNC_MANUAL)
				return false;//нет контроля положения стрелки
			if (Jinfo.state & zx_DSP_JunctionInfo.STATE_CUTUP)
				return false;//нет контроля положения стрелки
			if ((Jinfo.state & zx_DSP_JunctionInfo.STATE_MANUAL) and !Jinfo.IsDirection(innerProtectiveDir[i])){//стрелка заблокирована рукояткой
				return false;
			}
			if ((Jinfo.state & zx_DSP_JunctionInfo.STATE_LOCALCONTROL) and Jinfo.pathIndex != index and (state & POST_STATE_DSP))
				return false;//стрелка передана на другой пост
			if (Jinfo.slaveJunctions) {
				int j;
				int n = Jinfo.slaveJunctions.size();
				for (j = 0; j < n; ++j) {
					if (Jinfo.state & (zx_DSP_JunctionInfo.STATE_PATH | zx_DSP_JunctionInfo.STATE_SHUNT | zx_DSP_JunctionInfo.STATE_TRAIN | zx_DSP_JunctionInfo.STATE_LOCK)) {
						return false;//стрелка в маршруте
					}
					if (Jinfo.type & zx_DSP_JunctionInfo.JNC_MANUAL) {
						return false;//нет контроля положения стрелки
					}
					if (Jinfo.state & zx_DSP_JunctionInfo.STATE_CUTUP) {
						return false;//нет контроля положения стрелки
					}
				}
			}
		}
		for(i=0;i<outerProtective.size();i++){
			zx_DSP_JunctionInfo Jinfo = REdspCore.jncs[outerProtectiveIndex[i]];
			if(Jinfo.state&zx_DSP_JunctionInfo.STATE_LOCK)
				return false;//стрелка НЕ в маршруте, но проверку флага блокировки оставим.
			if (Jinfo.type & zx_DSP_JunctionInfo.JNC_MANUAL)
				return false;//нет контроля положения стрелки
			if (Jinfo.state & zx_DSP_JunctionInfo.STATE_CUTUP)
				return false;//нет контроля положения стрелки
			if ((Jinfo.state & zx_DSP_JunctionInfo.STATE_MANUAL) and !Jinfo.IsDirection(outerProtectiveDir[i])){//стрелка заблокирована рукояткой
				return false;
			}
			if ((Jinfo.state & zx_DSP_JunctionInfo.STATE_LOCALCONTROL) and Jinfo.pathIndex != index and (state & POST_STATE_DSP))
				return false;//стрелка передана на другой пост
		}
		if (state & POST_STATE_DSP) {
			for (i = 0; i < signals.size(); ++i) {
				zx_DSP_JunctionInfo Jinfo = REdspCore.FindFirstJunctionInfo(signals[i], false, false, true);
				if (Jinfo) {
					if (Jinfo.state & zx_DSP_JunctionInfo.STATE_PATH) {
						int dir = REdspCore.GetJunctionArrivalDirection(Jinfo.jnc, signals[i], false);
						if (Jinfo.pathdirection == zx_DSP_JunctionInfo.DIRECT_BACKWARD and dir == zx_DSP_JunctionInfo.JNC_BACK)
							return false;
						if (Jinfo.pathdirection == zx_DSP_JunctionInfo.DIRECT_FORWARD) {
							if (dir == zx_DSP_JunctionInfo.JNC_LEFT and Jinfo.jnc.GetDirection() == Junction.DIRECTION_LEFT)
								return false;
							if (dir == zx_DSP_JunctionInfo.JNC_RIGHT and Jinfo.jnc.GetDirection() == Junction.DIRECTION_RIGHT)
								return false;
						}
					}
				}
			}
		}
		ChangeState(token, state&POST_STATE_DSP);
		return true;
	}

	public string GetContentDSP(void){
		string ret="";
		if (post) {
			ret = ret + "<a href='live://Post" + index + "/users'>users list</a><br>";
		}

		ret = ret + "<font color='#";
		if(state&POST_STATE_FACT)
			ret=ret+"ffffff";
		else
			ret=ret+"000000";
		ret=ret+"'>*</font>&nbsp;";
		if (post) {
			ret = ret + "<font color='#";
			if (((state & POST_STATE_DSP) != 0) != ((state & POST_STATE_EXTERNAL) != 0))
				ret=ret+"ff0000";
			else
				ret=ret+"000000";
			ret=ret+"'>*</font>&nbsp;";
		}
		ret = ret + "<a href='live://Post"+index+"/";
		if(state&POST_STATE_DSP)
			ret=ret+"DSPdisable";
		else
			ret=ret+"DSPenable";
		ret=ret+"'>";
		if(state&POST_STATE_DSP)
			ret=ret+"<b>";
		ret=ret+"Разрешение манёвров";
		if(state&POST_STATE_DSP)
			ret=ret+"</b>";
		ret=ret+"</a><br>";
		if(state&POST_STATE_SEAL)
			ret=ret+"<a href='live://Post"+index+"/cancel'>";
		else
			ret=ret+"<i>";
		ret=ret+"Отмена манёвров";
		if(state&POST_STATE_SEAL)
			ret=ret+"</a>";
		else
			ret=ret+"</i>";
		ret=ret+"&nbsp;<a href='live://Post"+index+"/cancelseal'>*</a><br>";
		return ret;
	}

	void StateChanged(SecurityToken token)
	{
		Soup sp=Constructors.NewSoup();
		sp.SetNamedTag("function", "post");
		sp.SetNamedTag("post", index);
		sp.SetNamedTag("subfunc", "state");
		sp.SetNamedTag("state", state);
		MultiplayerGame.BroadcastGameplayMessage("suDSPjunctions", "sync", sp);
		if(((state&POST_STATE_DSP)!=0)==((state&POST_STATE_EXTERNAL)!=0) and ((state&POST_STATE_DSP)!=0)!=((state&POST_STATE_FACT)!=0)){
			if(!tryChangeState(token)){
				rule.waitPosts[0,1]=new int[1];
				rule.waitPosts[0]=index;
				if(!rule.postsCheckerStarted)
					rule.postsChecker();
			}
		}
		else if ((state & POST_STATE_FACT) and (((state&POST_STATE_DSP)!=0) != ((state&POST_STATE_EXTERNAL)!=0))) {
			int i;
			for(i = 0; i < signals.size(); i++) {
				if(signals[i].ex_sgn[1])
					signals[i].MainState=1;
				else
					signals[i].MainState=19;
				signals[i].SetSignal(true);
//				signals[i].Type=signals[i].Type|zxSignal.ST_UNTYPED;
			}
		}
		else if (state & POST_STATE_FACT) {
			int i;
			for(i = 0; i < signals.size(); i++){
//				signals[i].Type=signals[i].Type&~zxSignal.ST_UNTYPED;

				if(checkSignal(signals[i])){
					signals[i].MainState=20;
					signals[i].SetSignal(true);
				}
			}
		}
		if(rule.StationIndex==stationIndex)
			rule.UpdateDSPbrowser();
		if (post and post.browser)
			post.UpdateBrowser();
	}

	void DSPbtn(SecurityToken token, bool s){
		if(s)
			state=state|POST_STATE_DSP;
		else
			state=state&~POST_STATE_DSP;
		if (!post) {
			if(s)
				state=state|POST_STATE_EXTERNAL;
			else
				state=state&~POST_STATE_EXTERNAL;
		}
		StateChanged(token);
	}

	void ExternalBtn(SecurityToken token, bool s)
	{
		if(s)
			state=state|POST_STATE_EXTERNAL;
		else
			state=state&~POST_STATE_EXTERNAL;
		StateChanged(token);
	}

	public void SetExternalBtnState(bool s, SecurityToken token)
	{
		if(MultiplayerGame.IsActive() and !MultiplayerGame.IsServer()){
			if (!post or !post.isAvailableControl())
				return;
			Soup sp=Constructors.NewSoup();
			sp.SetNamedTag("function", "post");
			sp.SetNamedTag("post", index);
			sp.SetNamedTag("subfunc", "externalbtn");
			sp.SetNamedTag("state", s);
			MultiplayerGame.SendGameplayMessageToServer("suDSPjunctions", "sync", sp);
		}
		else {
			ExternalBtn(token, s);
			int code = LogEntry.CODE_POST_TAKE_ON;
			if (!s)
				code = LogEntry.CODE_POST_TAKE_OFF;
			rule.WriteLog(rule.REutility.GetUsername(), code, index, "");
		}
	}

	void CancelSeal(){
		state=state|POST_STATE_SEAL;
		Soup sp=Constructors.NewSoup();
		sp.SetNamedTag("function", "post");
		sp.SetNamedTag("post", index);
		sp.SetNamedTag("subfunc", "state");
		sp.SetNamedTag("state", state);
		MultiplayerGame.BroadcastGameplayMessage("suDSPjunctions", "sync", sp);
		if(rule.StationIndex==stationIndex)
			rule.UpdateDSPbrowser();
	}

	void Cancel(SecurityToken token){
		if(!(state&POST_STATE_SEAL))
			return;
		if(state&POST_STATE_DSP)//нет смысла нажимать отмену, если у самих включены манёвры
			return;
		int j;
		for(j=rule.waitPosts.size()-1;j>=0;j--)
			if(rule.waitPosts[j]==index)
				rule.waitPosts[j,j+1]=null;
		ChangeState(token, false);
	}

	public void Sync(Soup sp, SecurityToken token){
		string function=sp.GetNamedTag("subfunc");
		
		if(function=="state" and !MultiplayerGame.IsServer()){
			state=sp.GetNamedTagAsInt("state", state);
			if(rule.StationIndex==stationIndex)
				rule.UpdateDSPbrowser();
			if (post) {
				post.permissions = sp.GetNamedTagAsInt("post.permissions", post.permissions);
				if (post.browser)
					post.UpdateBrowser();
			}
		}
		else if(function=="dspbtn") {
			bool s = sp.GetNamedTagAsBool("state", false);
			DSPbtn(token, s);
			int code = LogEntry.CODE_POST_PERMISSION_ON;
			if (!s)
				code = LogEntry.CODE_POST_PERMISSION_OFF;
			rule.WriteLog(sp.GetNamedTag("__sender"), code, index, "");
		}
		else if(function=="externalbtn") {
			bool s = sp.GetNamedTagAsBool("state", false);
			ExternalBtn(token, s);
			int code = LogEntry.CODE_POST_TAKE_ON;
			if (!s)
				code = LogEntry.CODE_POST_TAKE_OFF;
			rule.WriteLog(sp.GetNamedTag("__sender"), code, index, "");
		}
		else if(function=="cancelseal"){
			CancelSeal();
			rule.WriteLog(sp.GetNamedTag("__sender"), LogEntry.CODE_POST_CANCEL_UNSEAL, index, "");
		}
		else if(function=="cancel"){
			Cancel(token);
			rule.WriteLog(sp.GetNamedTag("__sender"), LogEntry.CODE_POST_CANCEL_USE, index, "");
		}
	}

	public void syncState(string client) {
		Soup sp = Constructors.NewSoup();
		sp.SetNamedTag("function", "post");
		sp.SetNamedTag("post", index);
		sp.SetNamedTag("subfunc", "state");
		sp.SetNamedTag("state", state);
		if (post)
			sp.SetNamedTag("post.permissions", post.permissions);
		MultiplayerGame.SendGameplayMessageToClient(client, "suDSPjunctions", "sync", sp);
	}

	public void OnChangeText(string str, SecurityToken token){
		if(str=="DSPenable" or str=="DSPdisable"){
			bool s=false;
			if(str=="DSPenable")
				s=true;
			if(MultiplayerGame.IsActive() and !MultiplayerGame.IsServer()){
				Soup sp=Constructors.NewSoup();
				sp.SetNamedTag("function", "post");
				sp.SetNamedTag("post", index);
				sp.SetNamedTag("subfunc", "dspbtn");
				sp.SetNamedTag("state", s);
				MultiplayerGame.SendGameplayMessageToServer("suDSPjunctions", "sync", sp);
			}
			else{
				DSPbtn(token, s);
				int code = LogEntry.CODE_POST_PERMISSION_ON;
				if (!s)
					code = LogEntry.CODE_POST_PERMISSION_OFF;
				rule.WriteLog(rule.REutility.GetUsername(), code, index, "");
			}
		}
		else if(str=="cancelseal"){
			if(state&POST_STATE_SEAL and MultiplayerGame.IsActive() and !MultiplayerGame.IsServer())
				return;
			Menu menu=Constructors.NewMenu();
			menu.AddSeperator();
			if(state&POST_STATE_SEAL)
				menu.AddItem("Восстановить пломбу", rule, "Browser-URL", "live://Post"+index+"/cancelsealr");
			else
				menu.AddItem("Сорвать пломбу", rule, "Browser-URL", "live://Post"+index+"/cancelsealb");
			rule.DSPbrowser.PopupMenu(menu);
		}
		else if(str=="cancelsealb"){
			if(MultiplayerGame.IsActive() and !MultiplayerGame.IsServer()){
				Soup sp=Constructors.NewSoup();
				sp.SetNamedTag("function", "post");
				sp.SetNamedTag("post", index);
				sp.SetNamedTag("subfunc", "cancelseal");
				MultiplayerGame.SendGameplayMessageToServer("suDSPjunctions", "sync", sp);
			}
			else{
				CancelSeal();
				rule.WriteLog(rule.REutility.GetUsername(), LogEntry.CODE_POST_CANCEL_UNSEAL, index, "");
			}
		}
		else if(str=="cancelsealr"){
			if(MultiplayerGame.IsActive() and !MultiplayerGame.IsServer())
				return;
			state=state&~POST_STATE_SEAL;
			Soup sp=Constructors.NewSoup();
			sp.SetNamedTag("function", "post");
			sp.SetNamedTag("post", index);
			sp.SetNamedTag("subfunc", "state");
			sp.SetNamedTag("state", state);
			MultiplayerGame.BroadcastGameplayMessage("suDSPjunctions", "sync", sp);
			if(rule.StationIndex==stationIndex)
				rule.UpdateDSPbrowser();
			rule.WriteLog(rule.REutility.GetUsername(), LogEntry.CODE_POST_CANCEL_SEAL, index, "");
		}
		else if(str=="cancel"){
			if(!(state&POST_STATE_SEAL))
				return;
			if(MultiplayerGame.IsActive() and !MultiplayerGame.IsServer()){
				Soup sp=Constructors.NewSoup();
				sp.SetNamedTag("function", "post");
				sp.SetNamedTag("post", index);
				sp.SetNamedTag("subfunc", "cancel");
				MultiplayerGame.SendGameplayMessageToServer("suDSPjunctions", "sync", sp);
			}
			else{
				Cancel(token);
				rule.WriteLog(rule.REutility.GetUsername(), LogEntry.CODE_POST_CANCEL_USE, index, "");
			}
		}
		else if (str == "users") {
			if (post)
				post.requestAdminBrowser();
		}
	}

	public string GetDescriptionHTML(void){
		string ret="Внутренние охранные стрелки:<br><table>";
		int i;
		for(i=0;i<innerProtective.size();i++){
			ret=ret+"<tr>";
			zx_DSP_JunctionInfo Jinfo=null;
			if(REdspCore and innerProtectiveIndex[i] >= 0 and innerProtectiveIndex[i] < REdspCore.jncs.size())
				Jinfo = REdspCore.jncs[innerProtectiveIndex[i]];
			if (Jinfo and Jinfo.jnc != innerProtective[i]) {
				Jinfo = REdspCore.FindJunctionInfo(innerProtective[i], false, true);
			}
			if(Jinfo){
				ret = ret + "<td>#" + Jinfo.number + " " + Jinfo.name;
				if (Jinfo.stationId != REdspCore.stations[stationIndex].id) {
					ret = ret + " <font color='#ff0000'>@ ";
					int st = 0;
					while (st < REdspCore.stations.size() and REdspCore.stations[st].id != Jinfo.stationId)st++;
					if (st < REdspCore.stations.size())
						ret = ret + REdspCore.stations[st].name;
					else
						ret = ret + "???";
					ret = ret + "</font>";
				}
				ret = ret + "</td>";
				ret=ret+"<td><a href='live://property/Post"+index+"/InnerDir"+i+"'>";
				if(Jinfo.defaultDirection==innerProtectiveDir[i])
					ret=ret+"+";
				else
					ret=ret+"-";
				ret=ret+" (";
				if(innerProtectiveDir[i])
					ret=ret+"вправо";
				else
					ret=ret+"влево";
				ret=ret+")</a></td>";
			}
			else if (!REdspCore and innerProtective[i]) {
				ret = ret + "<td>" + innerProtective[i].GetName() + "</td>";
				ret = ret + "<td><a href='live://property/Post" + index + "/InnerDir" + i + "'>";
				if (innerProtectiveDir[i])
					ret = ret + "вправо";
				else
					ret = ret + "влево";
				ret = ret + "</a></td>";
			}
			else
				ret=ret+"<td colsspan=2><font color='#ff0000'>Стрелка не найдена</font></td>";
			ret=ret+"<td><a href='live://property/Post"+index+"/InnerDel"+i+"'>X</a></td>";
			ret=ret+"</tr>";
		}
		ret=ret+"<tr><td colsspan=3><a href='live://property/Post"+index+"/InnerAdd'>add</a></td></tr>";
		ret=ret+"</table><br>Внешние охранные стрелки:<br><table>";
		for(i=0;i<outerProtective.size();i++){
			ret=ret+"<tr>";
			zx_DSP_JunctionInfo Jinfo=null;
			if(REdspCore and outerProtectiveIndex[i] >= 0 and outerProtectiveIndex[i] < REdspCore.jncs.size())
				Jinfo = REdspCore.jncs[outerProtectiveIndex[i]];
			if (Jinfo and Jinfo.jnc != outerProtective[i]) {
				Jinfo = REdspCore.FindJunctionInfo(outerProtective[i], false, true);
			}
			if(Jinfo){
				ret = ret + "<td>#" + Jinfo.number + " " + Jinfo.name;
				if (Jinfo.stationId != REdspCore.stations[stationIndex].id) {
					ret = ret + " <font color='#ff0000'>@ ";
					int st = 0;
					while (st < REdspCore.stations.size() and REdspCore.stations[st].id != Jinfo.stationId)st++;
					if (st < REdspCore.stations.size())
						ret = ret + REdspCore.stations[st].name;
					else
						ret = ret + "???";
					ret = ret + "</font>";
				}
				ret = ret + "</td>";
				ret=ret+"<td><a href='live://property/Post"+index+"/OuterDir"+i+"'>";
				if(Jinfo.defaultDirection==outerProtectiveDir[i])
					ret=ret+"+";
				else
					ret=ret+"-";
				ret=ret+" (";
				if(outerProtectiveDir[i])
					ret=ret+"вправо";
				else
					ret=ret+"влево";
				ret=ret+")</a></td>";
			}
			else if (!REdspCore and outerProtective[i]) {
				ret = ret + "<td>" + outerProtective[i].GetName() + "</td>";
				ret = ret + "<td><a href='live://property/Post" + index + "/OuterDir" + i + "'>";
				if (outerProtectiveDir[i])
					ret = ret + "вправо";
				else
					ret = ret + "влево";
				ret = ret + "</a></td>";
			}
			else
				ret=ret+"<td colsspan=2><font color='#ff0000'>Стрелка не найдена</font></td>";
			ret=ret+"<td><a href='live://property/Post"+index+"/OuterDel"+i+"'>X</a></td>";
			ret=ret+"</tr>";
		}
		ret=ret+"<tr><td colsspan=3><a href='live://property/Post"+index+"/OuterAdd'>add</a></td></tr>";
		ret=ret+"</table><br>Светофоры:<br><table>";
		for(i=0;i<signals.size();i++){
			ret=ret+"<tr>";
			if(signals[i]){
				ret=ret+"<td>"+signals[i].privateName;
				if(REdspCore and signals[i].stationName != REdspCore.stations[stationIndex].name)
					ret=ret+" <font color='#ff0000'>@ "+signals[i].stationName+"</font>";
				ret=ret+" ("+signals[i].GetName()+")</td>";
			}
			else
				ret=ret+"<td colsspan=2><font color='#ff0000'>Светофор не найден</font></td>";
			ret=ret+"<td><a href='live://property/Post"+index+"/SignalDel"+i+"'>X</a></td>";
			ret=ret+"</tr>";
		}
		ret=ret+"<tr><td colsspan=3><a href='live://property/Post"+index+"/SignalAdd'>add</a></td></tr>";
		ret=ret+"</table>";
		ret = ret + "junctionPost: <a href='live://property/Post"+index+"/Post'>";
		if (post) ret = ret + post.GetName();
		else ret = ret + "<i>none</i>";
		ret = ret + "</a>";
		if (post) ret = ret + "&nbsp;<a href='live://property/Post"+index+"/PostIndex'>variant: " + (postIndex + 1) + "</a>";
		ret = ret + "<br>";
		return ret;
	}

	public string GetPropertyType(string id){
		if(id=="name")return "string";
		if(id=="InnerAdd")return "list";
		if(id[,8]=="InnerDir")return "link";
		if(id[,8]=="InnerDel")return "link";
		if(id=="OuterAdd")return "list";
		if(id[,8]=="OuterDir")return "link";
		if(id[,8]=="OuterDel")return "link";
		if(id=="SignalAdd")return "list";
		if(id[,9]=="SignalDel")return "link";
		if (id == "Post") return "string";//ToDo: хорошо бы выбирать из списка
		if (id == "PostIndex") {
			if (post) return "int,1," + post.GetPropertyValue("variantCount") + ",1";
			return "int";
		}
		return "link";
	}

	public string GetPropertyValue(string id){
		if(id=="name")return name;
		if (id == "PostIndex") return "" + (postIndex + 1);
		return "";
	}

	public void SetPropertyValue(string id, string value){
		if(id=="name") name=value;
		else if (id == "Post") {
			if ((post = cast<JunctionPost>Router.GetGameObject(value)) and postIndex < 0)
				postIndex = 0;
		}
	}

	public void SetPropertyValue(string id, int value)
	{
		if (id == "PostIndex") postIndex = value - 1;
	}

	public bool isAlreadyAdded(Junction junction) {
		int i;
		for (i = 0; i < innerProtective.size(); ++i) {
			if (innerProtective[i] == junction) {
				return true;
			}
		}
		for (i = 0; i < outerProtective.size(); ++i) {
			if (outerProtective[i] == junction) {
				return true;
			}
		}
		if (post) {
			int mask = 1;
			for (i = 0; i < postIndex; i++) mask = mask * 2;
			for (i = 0; i < post.junctions.size(); ++i) {
				if ((post.jncPermissions[i] & mask) and post.junctions[i] == junction) {
					return true;
				}
			}
		}
		return false;
	}

	public bool isAlreadyAdded(zxSignal sig) {
		int i;
		for (i = 0; i < signals.size(); ++i) {
			if (signals[i] == sig) {
				return true;
			}
		}
		return false;
	}

	int[] ElementListIndexes;
	public string[] GetPropertyElementList(string id){
		string[] ret=new string[0];
		if(id=="InnerAdd" or id=="OuterAdd"){
			int i;
			ElementListIndexes = new int[0];
			if (REdspCore) {
				zx_DSP_Station S=REdspCore.stations[stationIndex];
				int n = 0;
				for(i=0;i<S.jncs.size();i++){
					//ToDo: исключить из списка ручные стрелки
					if (isAlreadyAdded(S.jncs[i].jnc)) {
						continue;
					}
					ElementListIndexes[n] = S.jncs[i].index;
					ret[n] = "#" + S.jncs[i].number + " " + S.jncs[i].name;
					++n;
				}
				return ret;
			}
			Junction[] JMlist = World.GetJunctionList();
			int n = 0;
			for (i = 0; i < JMlist.size(); ++i) {
				if (isAlreadyAdded(JMlist[i])) {
					continue;
				}
				string name = JMlist[i].GetName();
				if (!name or name == "") {
					continue;
				}
				ret[n] = JMlist[i].GetName();
				ElementListIndexes[n] = JMlist[i].GetId();
				++n;
			}
			return ret;
		}
		if(id=="SignalAdd"){
			int i;
			ElementListIndexes = new int[0];
			if (REdspCore) {
				zx_DSP_Station S = REdspCore.stations[stationIndex];
				int n = 0;
				for(i = 0; i < S.signals.size(); i++) {
					if (isAlreadyAdded(S.signals[i])) {
						continue;
					}
					ElementListIndexes[n] = i;
					ret[n] = S.signals[i].privateName + " (" + S.signals[i].GetName() + ")";
					++n;
				}
				return ret;
			}
			Signal[] signals = World.GetSignalList();
			int n = 0;
			for (i = 0; i < signals.size(); ++i) {
				zxSignal sig = cast<zxSignal>signals[i];
				if (!sig or isAlreadyAdded(sig)) {
					continue;
				}
				ret[n] = sig.privateName + " @ " + sig.stationName + " (" + sig.GetName() + ")";
				ElementListIndexes[n] = sig.GetId();
				++n;
			}
			return ret;
		}
		return ret;
	}

	public void SetPropertyValue(string id, string value, int index){
		if(id=="InnerAdd"){
			int n = innerProtective.size();
			if (REdspCore) {
				if(ElementListIndexes[index]<0 or ElementListIndexes[index]>=REdspCore.jncs.size())
					return;
				zx_DSP_JunctionInfo Jinfo=REdspCore.jncs[ElementListIndexes[index]];
				innerProtective[n] = Jinfo.jnc;
				innerProtectiveIndex[n] = ElementListIndexes[index];
				innerProtectiveDir[n]=Jinfo.defaultDirection;
			}
			else {
				Junction JM = cast<Junction>Router.GetGameObject(ElementListIndexes[index]);
				if (!JM) {
					return;
				}
				innerProtective[n] = JM;
				innerProtectiveIndex[n] = -1;
				innerProtectiveDir[n] = JM.GetDirection();
			}
		}
		else if(id=="OuterAdd"){
			int n = outerProtective.size();
			if (REdspCore) {
				if(ElementListIndexes[index]<0 or ElementListIndexes[index]>=REdspCore.jncs.size())
					return;
				zx_DSP_JunctionInfo Jinfo=REdspCore.jncs[ElementListIndexes[index]];
				outerProtective[n] = Jinfo.jnc;
				outerProtectiveIndex[n] = ElementListIndexes[index];
				outerProtectiveDir[n]=Jinfo.defaultDirection;
			}
			else {
				Junction JM = cast<Junction>Router.GetGameObject(ElementListIndexes[index]);
				if (!JM) {
					return;
				}
				outerProtective[n] = JM;
				outerProtectiveIndex[n] = -1;
				outerProtectiveDir[n] = JM.GetDirection();
			}
		}
		else if(id=="SignalAdd"){
			if (REdspCore) {
				signals[signals.size()] = REdspCore.stations[stationIndex].signals[ElementListIndexes[index]];
			}
			else {
				zxSignal sig = cast<zxSignal>Router.GetGameObject(ElementListIndexes[index]);
				if (sig) {
					signals[signals.size()] = sig;
				}
			}
		}
	}

	public void LinkPropertyValue(string id){
		if(id[,8]=="InnerDir"){
			int n=Str.ToInt(id[8,]);
			if(n<0 or n>=innerProtectiveDir.size())
				return;
			if(innerProtectiveDir[n]!=0)
				innerProtectiveDir[n]=0;
			else
				innerProtectiveDir[n]=2;
		}
		else if(id[,8]=="InnerDel"){
			int n=Str.ToInt(id[8,]);
			if(n<0 or n>=innerProtective.size())
				return;
			innerProtective[n,n+1]=null;
			innerProtectiveIndex[n,n+1] = null;
			innerProtectiveDir[n,n+1]=null;
		}
		else if(id[,8]=="OuterDir"){
			int n=Str.ToInt(id[8,]);
			if(n<0 or n>=outerProtectiveDir.size())
				return;
			if(outerProtectiveDir[n]!=0)
				outerProtectiveDir[n]=0;
			else
				outerProtectiveDir[n]=2;
		}
		else if(id[,8]=="OuterDel"){
			int n=Str.ToInt(id[8,]);
			if(n<0 or n>=outerProtective.size())
				return;
			outerProtective[n,n+1]=null;
			outerProtectiveIndex[n,n+1] = null;
			outerProtectiveDir[n,n+1]=null;
		}
		else if(id[,9]=="SignalDel"){
			int n=Str.ToInt(id[9,]);
			if(n<0 or n>=signals.size())
				return;
			signals[n,n+1]=null;
		}
	}

	public Soup GetProperties(void){
		Soup soup = Constructors.NewSoup();
		int i;
		soup.SetNamedTag("station", stationIndex);
		soup.SetNamedTag("name", name);
		soup.SetNamedTag("innercount", innerProtective.size());
		for(i=0;i<innerProtective.size();i++){
			if (innerProtective[i]) {
				soup.SetNamedTag("innername." + i, innerProtective[i].GetName());
			}
			soup.SetNamedTag("innerindex." + i, innerProtectiveIndex[i]);
			soup.SetNamedTag("innerdir."+i, innerProtectiveDir[i]);
		}
		soup.SetNamedTag("outercount", outerProtective.size());
		for(i=0;i<outerProtective.size();i++){
			if (outerProtective[i]) {
				soup.SetNamedTag("outername." + i, outerProtective[i].GetName());
			}
			soup.SetNamedTag("outerindex." + i, outerProtectiveIndex[i]);
			soup.SetNamedTag("outerdir."+i, outerProtectiveDir[i]);
		}
		soup.SetNamedTag("sigcount", signals.size());
		for(i=0;i<signals.size();i++){
			soup.SetNamedTag("signame."+i, signals[i].GetName());
		}
		soup.SetNamedTag("state", state);
		if (post) {
			soup.SetNamedTag("post", post.GetName());
			soup.SetNamedTag("postindex", postIndex);
		}
		return soup;
	}

	public void SetProperties(Soup soup){
		stationIndex = soup.GetNamedTagAsInt("station");
		name=soup.GetNamedTag("name");
		int i, n;
		n=soup.GetNamedTagAsInt("innercount");
		innerProtective = new Junction[n];
		innerProtectiveIndex = new int[n];
		innerProtectiveDir=new int[n];
		for(i=0;i<n;i++){
			innerProtective[i] = cast<Junction>Router.GetGameObject(soup.GetNamedTag("innername." + i));
			innerProtectiveIndex[i] = soup.GetNamedTagAsInt("innerindex." + i);
			innerProtectiveDir[i]=soup.GetNamedTagAsInt("innerdir."+i);
		}
		n=soup.GetNamedTagAsInt("outercount");
		outerProtective = new Junction[n];
		outerProtectiveIndex = new int[n];
		outerProtectiveDir=new int[n];
		for(i=0;i<n;i++){
			outerProtective[i] = cast<Junction>Router.GetGameObject(soup.GetNamedTag("outername." + i));
			outerProtectiveIndex[i] = soup.GetNamedTagAsInt("outerindex." + i);
			outerProtectiveDir[i]=soup.GetNamedTagAsInt("outerdir."+i);
		}
		n=soup.GetNamedTagAsInt("sigcount");
		signals=new zxSignal[n];
		for(i=0;i<n;i++)
			signals[i]=cast<zxSignal>Router.GetGameObject(soup.GetNamedTag("signame."+i));
		state=soup.GetNamedTagAsInt("state");
		post = cast<JunctionPost>Router.GetGameObject(soup.GetNamedTag("post"));
		postIndex = soup.GetNamedTagAsInt("postindex", -1);
		if (post and postIndex >= 0) {
			post.postDescriptions[postIndex] = me;
			post.rule = rule;
			post.postIndex = index;
		}
	}

	public bool checkIndexes() {
		if (!REdspCore) {
			return true;
		}
		if (stationIndex < 0) 
			return false;
		if (stationIndex >= REdspCore.stations.size())
			return false;
		int i;
		for (i = 0; i < innerProtective.size(); ++i) {
			if (!innerProtective[i])
				return false;
			if (innerProtectiveIndex[i] < 0)
				return false;
			if (innerProtectiveIndex[i] >= REdspCore.jncs.size())
				return false;
			if (innerProtective[i] != REdspCore.jncs[innerProtectiveIndex[i]].jnc)
				return false;
			if (REdspCore.jncs[innerProtectiveIndex[i]].stationId != REdspCore.stations[stationIndex].id)
				return false;
		}
		for (i = 0; i < outerProtective.size(); ++i) {
			if (!outerProtective[i])
				return false;
			if (outerProtectiveIndex[i] < 0)
				return false;
			if (outerProtectiveIndex[i] >= REdspCore.jncs.size())
				return false;
			if (outerProtective[i] != REdspCore.jncs[outerProtectiveIndex[i]].jnc)
				return false;
			if (REdspCore.jncs[outerProtectiveIndex[i]].stationId != REdspCore.stations[stationIndex].id)
				return false;
		}
		for (i = 0; i < signals.size(); ++i) {
			if (!signals[i])
				return false;
			if (signals[i].stationName != REdspCore.stations[stationIndex].name)
				return false;
		}
		if (post) {
			if (post.stationIndex != stationIndex)
				return false;
		}
		return true;
	}

	public void tryFixIndexes() {
		if (!REdspCore) {
			return;
		}
		int i;
		for (i = 0; i < innerProtective.size(); ++i) {
			if (!innerProtective[i]) {
				if (innerProtectiveIndex[i] >= 0 and innerProtectiveIndex[i] < REdspCore.jncs.size())
					innerProtective[i] = REdspCore.jncs[innerProtectiveIndex[i]].jnc;
				else
					return;
			}
			if (innerProtectiveIndex[i] < 0 or innerProtectiveIndex[i] >= REdspCore.jncs.size() or innerProtective[i] != REdspCore.jncs[innerProtectiveIndex[i]].jnc) {
				zx_DSP_JunctionInfo Jinfo = REdspCore.FindJunctionInfo(innerProtective[i], false, true);
				if (!Jinfo)
					return;
				innerProtectiveIndex[i] = Jinfo.index;
			}
		}
		for (i = 0; i < outerProtective.size(); ++i) {
			if (!outerProtective[i]) {
				if (outerProtectiveIndex[i] >= 0 and outerProtectiveIndex[i] < REdspCore.jncs.size())
					outerProtective[i] = REdspCore.jncs[outerProtectiveIndex[i]].jnc;
				else
					return;
			}
			if (outerProtectiveIndex[i] < 0 or outerProtectiveIndex[i] >= REdspCore.jncs.size() or outerProtective[i] != REdspCore.jncs[outerProtectiveIndex[i]].jnc) {
				zx_DSP_JunctionInfo Jinfo = REdspCore.FindJunctionInfo(outerProtective[i], false, true);
				if (!Jinfo)
					return;
				outerProtectiveIndex[i] = Jinfo.index;
			}
		}
		int newStationIndex = stationIndex;
		if (newStationIndex >= REdspCore.stations.size())
			newStationIndex = 0;
		bool first = true;
		for (i = 0; i < innerProtectiveIndex.size(); ++i) {
			zx_DSP_JunctionInfo Jinfo = REdspCore.jncs[innerProtectiveIndex[i]];
			if (Jinfo.stationId != REdspCore.stations[newStationIndex].id) {
				if (first) {
					int st = 0;
					while (st < REdspCore.stations.size() and REdspCore.stations[st].id != Jinfo.stationId)st++;
					if (st < REdspCore.stations.size())
						newStationIndex = st;
					else
						return;
				}
				else
					return;
			}
			first = false;
		}
		for (i = 0; i < outerProtectiveIndex.size(); ++i) {
			zx_DSP_JunctionInfo Jinfo = REdspCore.jncs[outerProtectiveIndex[i]];
			if (Jinfo.stationId != REdspCore.stations[newStationIndex].id) {
				if (first) {
					int st = 0;
					while (st < REdspCore.stations.size() and REdspCore.stations[st].id != Jinfo.stationId)st++;
					if (st < REdspCore.stations.size())
						newStationIndex = st;
					else
						return;
				}
				else
					return;
			}
			first = false;
		}
		for (i = 0; i < signals.size(); ++i) {
			zxSignal sig = signals[i];
			if (!sig)
				return;
			if (sig.stationName != REdspCore.stations[newStationIndex].name) {
				if (first) {
					int st = 0;
					while (st < REdspCore.stations.size() and REdspCore.stations[st].name != sig.stationName)st++;
					if (st < REdspCore.stations.size())
						newStationIndex = st;
					else
						return;
				}
				else
					return;
			}
			first = false;
		}
		
		stationIndex = newStationIndex;
		if (post) {
			post.tryFixIndexes();
			post.stationIndex = stationIndex;
		}
		if (rule)
			rule.StationIndex = stationIndex;
	}
};