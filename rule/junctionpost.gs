include "buildable.gs"
include "zx_dsp_core.gs"
include "externalcontroll.gs"

class JunctionPost isclass Buildable {
	Asset self;
	public Junction[] junctions = new Junction[0];
	public int[] jncIndexes = new int[0];
	public int[] jncPermissions = new int[0];
	public define int JNC_MINUS = 0x01000000;
	public int permissions = 0;
	string stationName;
	public int stationIndex=-1;
	public int postIndex = -1;
	int variantCount;
	public suDSPJunctions rule;
	zx_DSP_Library REdspLib;
	zx_DSP_Core REdspCore;
	public ExternalControll[] postDescriptions = new ExternalControll[0];
	public Browser browser = null;
	Browser adminBrowser = null;
	bool availableControl = false;
	string[] users = new string[0];
	SecurityToken junctionToken;

	public void UpdateBrowser()
	{
		if (!browser)
			return;
		string ret = "";
		int i;
		if (stationIndex < 0 or stationIndex >= REdspCore.stations.size()) {
			browser.LoadHTMLString(self, "Пост не привязан к станции");
			return;
		}
		if (!availableControl) {
			ret = ret + "Нет доступа для управления постом<br>";
			zx_DSP_Station S = REdspCore.stations[stationIndex];
			if (S.availableControl or !MultiplayerGame.IsActive() or MultiplayerGame.IsServer()) {
				ret = ret + "Выдайте себе права через <a href=\"live://admin\">админку</a>";
			}
			else {
				ret = ret + "Обратитесь к ДСП станции " + S.name + ": ";
				for (i = 0; i < S.users.size(); ++i) {
					if (i != 0) {
						ret = ret + ", ";
					}
					ret = ret + S.users[i];
				}
			}
			browser.LoadHTMLString(self, ret);
			return;
		}
		for (i = 0; i < variantCount; i++) {
			ExternalControll desc = null;
			if (postDescriptions and postDescriptions.size() > i)
				desc = postDescriptions[i];
			ret = ret + "Вариант " + (i + 1) + ": <font color='#";
			if (desc and desc.state&desc.POST_STATE_FACT)
				ret=ret+"ffffff";
			else
				ret=ret+"000000";
			ret=ret+"'>*</font>&nbsp;<font color='#";
			if (desc and (((desc.state & desc.POST_STATE_DSP) != 0) != ((desc.state & desc.POST_STATE_EXTERNAL) != 0)))
				ret=ret+"ff0000";
			else
				ret=ret+"000000";
			ret = ret+"'>*</font>&nbsp;<a href='live://accept/"+i+"/";
			if (desc and desc.state&desc.POST_STATE_EXTERNAL)
				ret=ret+"disable";
			else
				ret=ret+"enable";
			ret=ret+"'>";
			if (desc and desc.state&desc.POST_STATE_EXTERNAL)
				ret=ret+"<b>";
			ret=ret+"Восприятие";
			if (desc and desc.state&desc.POST_STATE_EXTERNAL)
				ret=ret+"</b>";
			ret=ret+"</a><br>";
		}
		ret=ret+"<br>Junctions:<br>";
		ret=ret+"<table>";
		for(i = 0; i < jncIndexes.size(); ++i) {
			zx_DSP_JunctionInfo Jinfo = REdspCore.jncs[jncIndexes[i]];
			if (Jinfo.type & zx_DSP_JunctionInfo.JNC_MANUAL) continue;
			bool p = Jinfo.IsDirection(true);
			bool m = Jinfo.IsDirection(false);
			if (Jinfo.state & zx_DSP_JunctionInfo.STATE_CUTUP) {
				p = m = false;
			}
			ret = ret + "<tr><td align=\"center\"><a href='live://select/" + Jinfo.index + "'>#" + Jinfo.GetFullTitle() + "</a></td><td align=\"center\"><a href='live://jp/" + i + "'><font color=\"#";
			if ((jncPermissions[i] & permissions) and p) ret = ret + "00ff00";
			else ret = ret + "000000";
			ret = ret+"\">*</font></a>&nbsp;<font color=\"#";
			if ((jncPermissions[i] & permissions) and !p and !m) ret = ret + "ff0000";
			else ret = ret + "000000";
			ret=ret+"\">*</font>&nbsp;<a href='live://jm/" + i + "'><font color=\"#";
			if ((jncPermissions[i] & permissions) and m) ret = ret + "ffff00";
			else ret = ret + "000000";
			ret = ret + "\">*</font></a></td></tr><tr><td align=\"center\"><a href='live://select/" + Jinfo.index + "'>" + Jinfo.name + "</a></td><td align=\"center\"><a href='live://jmenu/" + i + "'>";
			if(jncPermissions[i] & JNC_MINUS)
				ret=ret+"/";
			else
				ret=ret+"\\";
			ret=ret+"</a></td></tr>";
		}
		ret=ret+"</table>";
		browser.LoadHTMLString(self, ret);
	}

	public void UpdateAdminBrowser() {
		if (!adminBrowser)
			return;
		if (!REdspCore) {
			adminBrowser.LoadHTMLString(self, "REdspCore not found");
			return;
		}
		if (!rule) {
			adminBrowser.LoadHTMLString(self, "rule not found");
			return;
		}
		if (stationIndex < 0 or stationIndex >= REdspCore.stations.size()) {
			adminBrowser.LoadHTMLString(self, "Пост не привязан к станции");
			return;
		}
		zx_DSP_Station S = REdspCore.stations[stationIndex];
		if (!S.availableControl and MultiplayerGame.IsActive() and !MultiplayerGame.IsServer()) {
			adminBrowser.LoadHTMLString(self, "Нет доступа для управления станцией");
			return;
		}
		string ret = "Пользователи:<br>";
		int i;
		string myName = World.GetLocalPlayerName();
		Str.ToLower(myName);
		for (i = 0; i < users.size(); ++i) {
			ret = ret + users[i] + " <a href=\"live://removeuser/" + users[i] + "\">X</a>";
			if (users[i] == myName)
				ret = ret + " (Я)";
			ret = ret + "<br>";
		}
		ret = ret + "<br>Добавить пользователя:<br>";
		int j = 0;
		while (j < users.size() and users[j] != myName) ++j;
		if (j >= users.size()) {
			ret = ret + "<a href=\"live://adduser/" + myName + "\">" + myName + "</a> (Я)<br><br>";
		}
		int n = rule.mpsessionlib.CountUsers();
		for (i = 0; i < n; ++i) {
			string user = rule.mpsessionlib.GetIndexedUsername(i);
			Str.ToLower(user);
			if (user == myName)
				continue;
			j = 0;
			while (j < users.size() and user != users[j]) ++j;
			if (j < users.size())
				continue;
			ret = ret + "<a href=\"live://adduser/" + user + "\">" + user + "</a><br>";
		}
		ret = ret + "<br><trainz-object width='150' height='17' style='text-line' align='left' max-chars='50' id='newUser'></trainz-object> <a href=\"live://newuser\">add</a>";
		adminBrowser.LoadHTMLString(self, ret);
	}

	public bool isAvailableControl() {
		return availableControl;
	}

	public bool isAvailableControl(string user) {
		Str.ToLower(user);
		int i;
		for (i = 0; i < users.size(); ++i) {
			if (users[i] == user) {
				return true;
			}
		}
		return false;
	}

	public void addUser(string user) {
		int i;
		for (i = 0; i < users.size(); i++)
			if (users[i] == user)
				return;
		users[users.size()] = user;
		string myName = World.GetLocalPlayerName();
		Str.ToLower(myName);
		if (myName == user) {
			availableControl = true;
			if (browser) {
				UpdateBrowser();
			}
		}
		if (adminBrowser) {
			UpdateAdminBrowser();
		}
	}

	void addUserRequest(string user) {
		addUser(user);
		if (MultiplayerGame.IsActive()) {
			Soup sp = Constructors.NewSoup();
			sp.SetNamedTag("function", "junctionpost");
			sp.SetNamedTag("post", postIndex);
			sp.SetNamedTag("subfunc", "adduser");
			sp.SetNamedTag("user", user);
			MultiplayerGame.BroadcastGameplayMessage("suDSPjunctions", "sync", sp);
		}
	}

	public void removeUser(string user) {
		int i = 0;
		while (i < users.size()) {
			if (users[i] == user) {
				users[i,i+1] = null;
			}
			else
				++i;
		}
		string myName = World.GetLocalPlayerName();
		Str.ToLower(myName);
		if (myName == user) {
			availableControl = false;
			if (browser) {
				UpdateBrowser();
			}
		}
		if (adminBrowser) {
			UpdateAdminBrowser();
		}
	}

	void removeUserRequest(string user) {
		removeUser(user);
		if (MultiplayerGame.IsActive()) {
			Soup sp = Constructors.NewSoup();
			sp.SetNamedTag("function", "junctionpost");
			sp.SetNamedTag("post", postIndex);
			sp.SetNamedTag("subfunc", "removeuser");
			sp.SetNamedTag("user", user);
			MultiplayerGame.BroadcastGameplayMessage("suDSPjunctions", "sync", sp);
		}
	}

	int[] waitJunctions = new int[0];
	bool jPosCheckerStarted = false;
	thread void jPosChecker() {
		if (jPosCheckerStarted)
			return;
		jPosCheckerStarted = true;
		while (waitJunctions.size() > 0) {
			int j;
			for (j = waitJunctions.size()-1; j>=0; j--) {
				if (waitJunctions[j] < 0 or waitJunctions[j] >= jncIndexes.size())
					continue;
				if (jncIndexes[waitJunctions[j]] < 0 or jncIndexes[waitJunctions[j]] >= REdspCore.jncs.size())
					continue;
				zx_DSP_JunctionInfo Jinfo = REdspCore.jncs[jncIndexes[waitJunctions[j]]];
				if (Jinfo.state & (zx_DSP_JunctionInfo.STATE_PATH | zx_DSP_JunctionInfo.STATE_SHUNT | zx_DSP_JunctionInfo.STATE_TRAIN | zx_DSP_JunctionInfo.STATE_LOCK))
					continue;
				Jinfo.SetDirection(junctionToken, !(jncPermissions[waitJunctions[j]] & JNC_MINUS));
				waitJunctions[j,j+1] = null;
			}
			Sleep(0.1);
		}
		jPosCheckerStarted = false;
	}

	void jPos (int index, int pos) {
		if (index < 0 or index >= jncIndexes.size())
			return;
		pos = pos & JNC_MINUS;
		jncPermissions[index] = jncPermissions[index] & ~JNC_MINUS;
		jncPermissions[index] = jncPermissions[index] | pos;
		Soup sp = Constructors.NewSoup();
		sp.SetNamedTag("function", "junctionpost");
		sp.SetNamedTag("post", postIndex);
		sp.SetNamedTag("subfunc", "jstate");
		sp.SetNamedTag("junction", index);
		sp.SetNamedTag("state", jncPermissions[index]);
		MultiplayerGame.BroadcastGameplayMessage("suDSPjunctions", "sync", sp);
		if (jncIndexes[index] < 0 or jncIndexes[index] >= REdspCore.jncs.size() or (jncPermissions[index] & permissions) == 0) {
			if (browser)
				UpdateBrowser();
			return;
		}
		zx_DSP_JunctionInfo Jinfo = REdspCore.jncs[jncIndexes[index]];
		if (Jinfo.state & (zx_DSP_JunctionInfo.STATE_PATH | zx_DSP_JunctionInfo.STATE_SHUNT | zx_DSP_JunctionInfo.STATE_TRAIN | zx_DSP_JunctionInfo.STATE_LOCK)){
			waitJunctions[0,1] = new int[1];
			waitJunctions[0] = index;
			if (!jPosCheckerStarted)
				jPosChecker();
		}
		else {//можно переводить
			int j;
			for (j = waitJunctions.size() - 1; j >= 0; j--)
				if (waitJunctions[j] == index)
					waitJunctions[j,j+1] = null;
			Jinfo.SetDirection(junctionToken, !(pos & JNC_MINUS));
		}
		if (browser)
			UpdateBrowser();
	}
	public void jPos(int index) {
		jPos(index, jncPermissions[index]);
	}

	public void Sync(Soup sp) {
		string function = sp.GetNamedTag("subfunc");

		if (function == "jstate" and !MultiplayerGame.IsServer()) {
			int n = sp.GetNamedTagAsInt("junction", -1);
			if (n < 0 or n >= jncPermissions.size())
				return;
			jncPermissions[n] = sp.GetNamedTagAsInt("state", jncPermissions[n]);
			if (browser)
				UpdateBrowser();
		}
		else if (function == "jpos") {
			int n = sp.GetNamedTagAsInt("junction", -1);
			int pos = sp.GetNamedTagAsInt("pos", 0);
			jPos(n, pos);
			if (rule)
				rule.WriteLog(sp.GetNamedTag("__sender"), LogEntry.CODE_POST_SWITCH, n, "");
		}
		else if (function == "adduser" and !MultiplayerGame.IsServer()) {
			addUser(sp.GetNamedTag("user"));
		}
		else if (function == "adduserrequest" and MultiplayerGame.IsServer()) {
			addUserRequest(sp.GetNamedTag("user"));
		}
		else if (function == "removeuser" and !MultiplayerGame.IsServer()) {
			removeUser(sp.GetNamedTag("user"));
		}
		else if (function == "removeuserrequest" and MultiplayerGame.IsServer()) {
			removeUserRequest(sp.GetNamedTag("user"));
		}
	}

	public void syncState(string client) {
		int i;
		for (i = 0; i < jncPermissions.size(); ++i) {
			Soup sp = Constructors.NewSoup();
			sp.SetNamedTag("function", "junctionpost");
			sp.SetNamedTag("post", postIndex);
			sp.SetNamedTag("subfunc", "jstate");
			sp.SetNamedTag("junction", i);
			sp.SetNamedTag("state", jncPermissions[i]);
			MultiplayerGame.SendGameplayMessageToClient(client, "suDSPjunctions", "sync", sp);
		}
		for (i = 0; i < users.size(); ++i) {
			Soup sp = Constructors.NewSoup();
			sp.SetNamedTag("function", "junctionpost");
			sp.SetNamedTag("post", postIndex);
			sp.SetNamedTag("subfunc", "adduser");
			sp.SetNamedTag("user", users[i]);
			MultiplayerGame.BroadcastGameplayMessage("suDSPjunctions", "sync", sp);
		}
	}

	public void requestAdminBrowser() {
		if (adminBrowser) {
			if (adminBrowser.IsWindowMinimised())
				adminBrowser.RestoreWindow();
		}
		else {
			adminBrowser = Constructors.NewBrowser();
			adminBrowser.SetWindowRect(10, 75, 10 + 500, 75 + 300);
			adminBrowser.SetWindowStyle(Browser.STYLE_DEFAULT);
			adminBrowser.SetScrollEnabled(true);
			adminBrowser.SetCloseEnabled(true);
//			adminBrowser.SetWindowTitle(ST.GetString("log-browser-title"));
//			adminBrowser.SetButtonOverlayStyle(Browser.BS_OK, Browser.BS_None);
			adminBrowser.SetRememberPosition(self, "post-admin-browser");
			adminBrowser.SetWindowGrow(500, 400, Interface.GetDisplayWidth(), Interface.GetDisplayHeight());
		}
		UpdateAdminBrowser();
		adminBrowser.SetWindowVisible(true);
	}

	public void ChangeText(Message msg){
		if(msg.src == browser){
			if(msg.minor=="live://refresh")
				UpdateBrowser();
			else if(msg.minor[,14]=="live://accept/"){
				int n = Str.Find(msg.minor, "/", 14);
				if(n>0){
					int k = Str.ToInt(msg.minor[14,n]);
					if(k >= 0 and k < postDescriptions.size() and postDescriptions[k]){
						bool s = false;
						if (msg.minor[n+1,] == "enable")
							s = true;
						postDescriptions[k].SetExternalBtnState(s, junctionToken);
					}
				}
				UpdateBrowser();
			}
			else if (msg.minor[,10] == "live://jp/" or msg.minor[,10] == "live://jm/") {
				if (!availableControl)
					return;
				int n = Str.ToInt(msg.minor[10,]);
				if (n < 0 or n >= jncPermissions.size())
					return;

				int pos = 0;
				if (msg.minor[8,9]=="m")
					pos = JNC_MINUS;
				if(MultiplayerGame.IsActive() and !MultiplayerGame.IsServer()){
					Soup sp=Constructors.NewSoup();
					sp.SetNamedTag("function", "junctionpost");
					sp.SetNamedTag("post", postIndex);
					sp.SetNamedTag("subfunc", "jpos");
					sp.SetNamedTag("junction", n);
					sp.SetNamedTag("pos", pos);
					MultiplayerGame.SendGameplayMessageToServer("suDSPjunctions", "sync", sp);
				}
				else{
					jPos(n, pos);
					if (rule)
						rule.WriteLog(rule.REutility.GetUsername(), LogEntry.CODE_POST_SWITCH, n, "");
				}
//				if (isMinus)
//					jncPermissions[n] = jncPermissions[n] | JNC_MINUS;
//				else
//					jncPermissions[n] = jncPermissions[n] & ~JNC_MINUS;
//				UpdateBrowser();
			}
			else if(msg.minor[,13]=="live://jmenu/"){
				int n = Str.ToInt(msg.minor[13,]);
				Menu menu = Constructors.NewMenu();
				menu.AddSeperator();
				menu.AddItem("Перевести рукоятку влево", me, "Browser-URL", "live://jp/" + n);
				menu.AddItem("Перевести рукоятку вправо", me, "Browser-URL", "live://jm/" + n);
				browser.PopupMenu(menu);
			}
			else if (msg.minor == "live://admin") {
				requestAdminBrowser();
			}
		}
		else if (msg.src == adminBrowser) {
			if (msg.minor[,15] == "live://adduser/" or msg.minor == "live://newuser") {
				string user;
				if (msg.minor == "live://newuser") {
					user = adminBrowser.GetElementProperty("newUser", "text");
					Str.ToLower(user);
				}
				else
					user = msg.minor[15,];
				if (MultiplayerGame.IsActive() and !MultiplayerGame.IsServer()) {
					Soup sp=Constructors.NewSoup();
					sp.SetNamedTag("function", "junctionpost");
					sp.SetNamedTag("post", postIndex);
					sp.SetNamedTag("subfunc", "adduserrequest");
					sp.SetNamedTag("user", user);
					MultiplayerGame.SendGameplayMessageToServer("suDSPjunctions", "sync", sp);
				}
				else {
					addUserRequest(user);
				}
			}
			else if (msg.minor[,18] == "live://removeuser/") {
				string user = msg.minor[18,];
				if (MultiplayerGame.IsActive() and !MultiplayerGame.IsServer()) {
					Soup sp=Constructors.NewSoup();
					sp.SetNamedTag("function", "junctionpost");
					sp.SetNamedTag("post", postIndex);
					sp.SetNamedTag("subfunc", "removeuserrequest");
					sp.SetNamedTag("user", user);
					MultiplayerGame.SendGameplayMessageToServer("suDSPjunctions", "sync", sp);
				}
				else {
					removeUserRequest(user);
				}
			}
		}
	}

	void BrowserClosed(Message msg){
		if(msg.src == browser)
			browser = null;
		else if (msg.src == adminBrowser)
			adminBrowser = null;
	}

	void ViewDetails(Message msg){
		if (browser){
			if (browser.IsWindowMinimised())
				browser.RestoreWindow();
		}
		else {
			browser = Constructors.NewBrowser();
			browser.SetWindowRect(10,75,10+500,75+300);
			browser.SetWindowStyle(Browser.STYLE_DEFAULT);
			browser.SetScrollEnabled(true);
			browser.SetCloseEnabled(true);
//			browser.SetWindowTitle(ST.GetString("log-browser-title"));
//			browser.SetButtonOverlayStyle(Browser.BS_OK, Browser.BS_None);
			browser.SetRememberPosition(self, "post-browser");
			browser.SetWindowGrow(500, 400, Interface.GetDisplayWidth(), Interface.GetDisplayHeight());
		}
		UpdateBrowser();
		browser.SetWindowVisible(true);
	}

	public string GetDescriptionHTML(void){
		zx_DSP_Core REdspCore;
		zx_DSP_Station S = null;
		string ret = "";

		if(REdspLib and (REdspCore=cast<zx_DSP_Core>REdspLib.GetController())){
			if(stationIndex>=0 and stationIndex<REdspCore.stations.size())
				S = REdspCore.stations[stationIndex];
			ret=ret+"station: <a href=\"live://property/station\">";
			if (S)
				ret=ret+S.name;
			else
				ret=ret+"<i>none</i>";
			ret=ret+"</a><br>";
		}
		else{
			ret=ret+"DSP controller not found<br>";
		}
		ret=ret+"variantCount: <a href=\"live://property/variantCount\">"+variantCount+"</a><br>";
		ret = ret + "variants:<br>";
		int i, j;
		for (i = 0; i < postDescriptions.size(); i++) {
			ret = ret + postDescriptions[i].name + " <a href=\"live://property/delVariant"+i+"\">X</a><br>";
		}
		ret = ret + "<a href=\"live://property/addVariant\">add</a><br>";
		ret=ret+"junctions:<br>";
		ret=ret+"<table><tr><td>junction</td>";
		for (j=0;j<variantCount;j++)
			ret=ret+"<td>"+(j+1)+"</td>";
		ret=ret+"<td>&nbsp;</td></tr>";
		for (i=0; i<junctions.size(); i++){
			ret=ret+"<tr><td>";
			zx_DSP_JunctionInfo Jinfo = null;
			if (REdspCore and jncIndexes[i]>=0 and jncIndexes[i]<REdspCore.jncs.size())
				Jinfo = REdspCore.jncs[jncIndexes[i]];
			if (Jinfo and Jinfo.jnc != junctions[i])
				Jinfo = REdspCore.FindJunctionInfo(junctions[i], false, true);
			if (Jinfo){
				ret=ret+"#"+Jinfo.number;
				if (!S or S.id!=Jinfo.stationId){
					int k = 0;
					while (k<REdspCore.stations.size() and REdspCore.stations[k].id!=Jinfo.stationId)k++;
					ret=ret+"&nbsp;<font color=\"#ff0000\">@&nbsp;";
					if (k<REdspCore.stations.size())
						ret=ret+REdspCore.stations[k].name;
					else
						ret=ret+"<i>uncknown</i>";
					ret=ret+"</font>";
				}
				ret=ret+" ("+Jinfo.name+")";
			}
			else if(junctions[i])
				ret=ret+junctions[i].GetName();
			else
				ret=ret+"<i>uncknown</i>";
			ret=ret+"</td>";
			int m = 1;
			for (j=0;j<variantCount;j++){
				ret=ret+"<td>"+HTMLWindow.CheckBox("live://property/permission"+i+"^"+m, jncPermissions[i] & m)+"</td>";
				m = m*2;
			}
			ret=ret+"<td><a href=\"live://property/delJunction"+i+"\">X</a></td></tr>";
		}
		ret=ret+"<tr><td><a href=\"live://property/addJunction\">add</a></td><td colsspan=\""+(variantCount+1)+"\">&nbsp;</td></tr></table>";
		return ret;
	}

	string GetPropertyType(string id){
		if (id == "station") return "list,1";
		if (id == "variantCount") return "int,1,24";
		if (id == "addJunction") return "list";
		if (id[,10] == "permission") return "link";
		if (id[,11] == "delJunction") return "link";
		if (id == "addVariant") return "list";
		if (id[,10] == "delVariant") return "link";
		return inherited(id);
	}

	public string GetPropertyValue(string id){
		if (id == "variantCount") return "" + variantCount;
		return inherited(id);
	}

	public bool isAlreadyAdded(Junction junction) {
		int i;
		for (i = 0; i < junctions.size(); ++i) {
			if (junctions[i] == junction) {
				return true;
			}
		}
		return false;
	}

	Junction[] elementListJunctions;
	int[] elementListIndexes;
	public string[] GetPropertyElementList(string id){
		if (id == "station"){
			zx_DSP_Core REdspCore;
			string[] ret = new string[0];
			int i;
			if(!REdspLib or !(REdspCore=cast<zx_DSP_Core>REdspLib.GetController()))
				return ret;

			for (i=0;i<REdspCore.stations.size();i++)
				ret[i] = REdspCore.stations[i].name;
			return ret;
		}
		else if (id == "addJunction"){
			zx_DSP_Core REdspCore;
			zx_DSP_Station S = null;
			string[] ret = new string[0];
			elementListJunctions = new Junction[0];
			elementListIndexes = new int[0];
			if(!REdspLib or !(REdspCore=cast<zx_DSP_Core>REdspLib.GetController())){
				Junction[] J = World.GetJunctionList();
				int i;
				int n = 0;
				for (i=0; i<J.size(); i++){
					if (isAlreadyAdded(J[i])) {
						continue;
					}
					ret[n] = J[i].GetName();
					elementListJunctions[n] = J[i];
					elementListIndexes[n] = -1;
					++n;
				}
				return ret;
			}
			if (stationIndex<0 or stationIndex>=REdspCore.stations.size())
				return ret;
			S=REdspCore.stations[stationIndex];
			int i;
			int n = 0;
			for (i=0; i<S.jncs.size(); i++){
				if (S.jncs[i].masterJunction) {
					continue;
				}
				if (isAlreadyAdded(S.jncs[i].jnc)) {
					continue;
				}
				ret[n] = S.jncs[i].GetFullTitle() +" ("+S.jncs[i].name+")";
				elementListJunctions[n] = S.jncs[i].jnc;
				elementListIndexes[n] = S.jncs[i].index;
				++n;
			}
			return ret;
		}
		else if (id == "addVariant") {
			
		}
		return inherited(id);
	}

	void SetPropertyValue(string id, string value, int index){
		if (id == "station") stationIndex = index;
		else if (id == "addJunction"){
			int n = junctions.size();
			junctions[n] = elementListJunctions[index];
			jncIndexes[n] = elementListIndexes[index];
			jncPermissions[n] = 0;
		}
		else inherited(id, value, index);
	}

	void SetPropertyValue(string id, int value){
		if (id == "variantCount") variantCount = value;
		else inherited(id, value);
	}

	void LinkPropertyValue(string id){
		if (id[,10] == "permission"){
			string[] param = Str.Tokens(id[10,], "^");
			if (param.size()<2)
				return;
			int i = Str.ToInt(param[0]);
			int m = Str.ToInt(param[1]);
			if (i>=0 and i<jncPermissions.size())
				jncPermissions[i] = jncPermissions[i] ^ m;
		}
		else if (id[,11] == "delJunction"){
			int n = Str.ToInt(id[11,]);
			if (n>=0 and n<junctions.size()){
				junctions[n,n+1] = null;
				jncIndexes[n,n+1] = null;
				jncPermissions[n,n+1] = null;
			}
		}
		else inherited(id);
	}

	public Soup GetProperties(void){
		Soup soup = inherited();
		soup.SetNamedTag("station", stationIndex);
		soup.SetNamedTag("variantcount", variantCount);
		soup.SetNamedTag("permissions", permissions);
		soup.SetNamedTag("junctionscount", junctions.size());
		int i;
		for (i=0; i<junctions.size(); i++){
			soup.SetNamedTag("junctionname."+i, junctions[i].GetName());
			soup.SetNamedTag("junctionindex."+i, jncIndexes[i]);
			soup.SetNamedTag("junctionpermissions."+i, jncPermissions[i]);
		}
		return soup;
	}

	public void SetProperties(Soup soup){
		inherited(soup);
		stationIndex = soup.GetNamedTagAsInt("station", -1);
		variantCount = soup.GetNamedTagAsInt("variantcount", 1);
		permissions = soup.GetNamedTagAsInt("permissions", 0);
		int n = soup.GetNamedTagAsInt("junctionscount", 0);
		int i;
		junctions = new Junction[0];
		jncIndexes = new int[0];
		jncPermissions = new int[0];
		for (i=0; i<n; i++){
			junctions[i] = cast<Junction>Router.GetGameObject(soup.GetNamedTag("junctionname."+i));
			jncIndexes[i] = soup.GetNamedTagAsInt("junctionindex."+i, -1);//ToDo: в moduleInitHandler'е сделать проверку и пересчёт индекса
			jncPermissions[i] = soup.GetNamedTagAsInt("junctionpermissions."+i, 0);
		}
/*
		ScenarioBehavior[] rules = World.GetBehaviors();
		for (i=0; i<rules.size(); i++) {
			if (rules[i].GetGSClassName() == "suDSPJunctions") {
				rule = rules[i];
			}
		}*/
	}

	void OnJunction(Message msg) {
		if (browser)
			UpdateBrowser();
	}

	public bool checkIndexes() {
		if (!REdspCore)
			return true;
		int i;
		for (i = 0; i < junctions.size(); ++i) {
			if (!junctions[i])
				return false;
			if (jncIndexes[i] < 0)
				return false;
			if (jncIndexes[i] >= REdspCore.jncs.size())
				return false;
			if (junctions[i] != REdspCore.jncs[jncIndexes[i]].jnc)
				return false;
			if (REdspCore.jncs[jncIndexes[i]].stationId != REdspCore.stations[stationIndex].id)
				return false;
		}
		return true;
	}

	public void tryFixIndexes() {
		if (!REdspCore)
			return;
		int i;
		for (i = 0; i < junctions.size(); ++i) {
			if (!junctions[i]) {
				if (jncIndexes[i] >=0 and jncIndexes[i] < REdspCore.jncs.size())
					junctions[i] = REdspCore.jncs[jncIndexes[i]].jnc;
				else
					return;
			}
			if (jncIndexes[i] < 0 or jncIndexes[i] >= REdspCore.jncs.size() or junctions[i] != REdspCore.jncs[jncIndexes[i]].jnc) {
				zx_DSP_JunctionInfo Jinfo = REdspCore.FindJunctionInfo(junctions[i], false, true);
				if (!Jinfo)
					return;
				jncIndexes[i] = Jinfo.index;
			}
		}
	}

	void ModuleInitHandler(Message msg){
		if (World.GetCurrentModule() == World.DRIVER_MODULE){
			if (REdspLib and (REdspCore = cast<zx_DSP_Core>REdspLib.GetController())){
				AddHandler(me, "Junction", "Toggled", "OnJunction");
//				if (!checkIndexes()) {
//					tryFixIndexes();
//				}
			}
		}
	}

	public void Init(Asset asset){
		inherited(self=asset);
		REdspLib = cast<zx_DSP_Library>TrainzScript.GetLibrary(self.LookupKUIDTable("REdspLib"));
		AddHandler(me, "MapObject", "View-Details", "ViewDetails");
		AddHandler(me, "Browser-URL", "", "ChangeText");
		AddHandler(me, "Browser-Closed", "", "BrowserClosed");
		AddHandler(me, "World", "ModuleInit", "ModuleInitHandler");
		string[] rights = new string[1];
		rights[0] = "junction-dir";
		junctionToken = IssueSecurityToken(asset.GetKUID(), rights);
	}
};