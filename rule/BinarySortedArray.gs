include "gs.gs"
include "common.gs"

class BinarySortedElement{
	public int key;
	public int type;
	public int state;
};


class BinarySortedArray{
	BinarySortedElement[] DBSE=new BinarySortedElement[0];	// основной массив элементов

	public int count() {
		return DBSE.size();
	}

	public BinarySortedElement getElementByIndex(int index) {
		return DBSE[index];
	}

	int find(int a, bool mode){ // при mode = true указывает место, где мог бы находиться новый элемент
		int i = 0;
		int f = 0;
		int b = DBSE.size() - 1;
		if (DBSE.size() > 0) {
			if(DBSE[f].key == a)
				return f;
			if(DBSE[b].key == a)
				return b;

			if(a<DBSE[f].key){
				if(mode)
					return 0;
				else
					return -1;
			}
			if(DBSE[b].key<a){
				if(mode)
					return DBSE.size();
				else
					return -1;
			}
			while(b>(f+1)){
				i=f + (int)((b-f)/2);				// середина отрезка

				if(DBSE[i].key==a)
					return i;

				if( DBSE[f].key<a and a<DBSE[i].key)	// на отрезке от f до i
					b=i;
				if( DBSE[i].key<a and a<DBSE[b].key)	// на отрезке от i до b
					f=i;
			}
			if(DBSE[f+1].key==a or (mode and DBSE[f].key<a and a<DBSE[f+1].key))
				return f+1;

			if(mode and DBSE[f+1].key<a and a<DBSE[f+2].key)
				return f+2;
		}

		if(mode)
			return i;
		return -1;					// не найден
	}

	public int findElementIndex(int key) {
		return find(key, false);
	}

	public BinarySortedElement findElement(int key) {
		int t = find(key, false);
		if (t >= 0 and t < DBSE.size())
			return DBSE[t];
		return null;
	}

	public int addElement(BinarySortedElement element) {
		int t = find(element.key, true);
		DBSE[t,t] = new BinarySortedElement[1];
		DBSE[t] = element;
		return t;
	}

	public void DeleteElementByNmb(int a){
		if(a>=0){
			DBSE[a,a+1] = null;
		}
	}

	public void DeleteElement(int a){
		DeleteElementByNmb(find(a,false));
	}

	public void Clear(){
		DBSE = new BinarySortedElement[0];
	}
};
